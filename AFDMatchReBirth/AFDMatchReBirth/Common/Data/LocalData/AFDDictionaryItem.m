//
//  AFDDictionaryItem.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDDictionaryItem.h"

@implementation AFDDictionaryItem

+ (id)dictionaryItemWithKey:(NSString *)value andText:(NSString *)text {
    AFDDictionaryItem *item = [[AFDDictionaryItem alloc] init];
    item.key = value;
    item.text = text;
    return item;
}

+ (id)dictionaryItemWithKey:(NSString *)value andText:(NSString *)text andDescText:(NSString *)desc {
    AFDDictionaryItem *item = [[AFDDictionaryItem alloc] init];
    item.key = value;
    item.text = text;
    item.descText = desc;
    return item;
}

+ (id)dictionaryItemWithKey:(NSString *)value andText:(NSString *)text andDescText:(NSString *)desc andIsClearOption:(BOOL)option {
    AFDDictionaryItem *item = [[AFDDictionaryItem alloc] init];
    item.key = value;
    item.text = text;
    item.descText = desc;
    item.isClearOption = option;
    return item;
}

- (BOOL)isEqualToItem:(AFDDictionaryItem *)item {
    if(self.key == nil && item && item.key == nil){
        if (self.text == nil && item.key == nil) {
            return YES;
        }
        return [self.text isEqualToString:item.text];
    }else{
        if ((item.key == nil || self.key == nil) && item) {
            return [self.text isEqualToString:item.text];
        } else {
            return [self.key isEqualToString:item.key];
        }
    }
}

@end
