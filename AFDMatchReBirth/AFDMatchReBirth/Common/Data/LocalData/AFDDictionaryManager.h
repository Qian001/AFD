//
//  AFDDictionaryManager.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "AFDDictionaryItem.h"
#import "AFDFMDBManager.h"

@interface AFDDictionaryManager : NSObject

+ (NSArray<AFDDictionaryItem*> *)genderDictionaryItems;


// Location

+ (NSArray<AFDDictionaryItem*> *)countryDictionaryItems;
+ (AFDDictionaryItem *)countryFromPlacemark:(CLPlacemark *)place;
+ (NSArray<AFDDictionaryItem*> *)stateDictionaryOfCountry:(AFDDictionaryItem *)country;
+ (AFDDictionaryItem *)stateOfCountry:(AFDDictionaryItem *)country fromPlacemark:(CLPlacemark *)place;

@end
