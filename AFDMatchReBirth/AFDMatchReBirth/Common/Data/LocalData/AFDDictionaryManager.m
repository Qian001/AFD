//
//  AFDDictionaryManager.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDDictionaryManager.h"

@implementation AFDDictionaryManager

+ (NSArray *)dictionaryFromAllArrayFormatFileNamed:(NSString *)name {
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:@"local"];
    if (path == nil) {
        path = [[NSBundle bundleForClass:[self class]] pathForResource:name ofType:@"local"];
    }
    NSString *source = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSData *data = [source dataUsingEncoding:NSUTF8StringEncoding];
    if (data == nil) {
        return nil;
    }
    NSError *error;
    id result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    if (error != nil) return nil;
    return  [self msDictionaryItemsfor:result];
}

+ (NSArray *)msDictionaryItemsfor:(NSArray *)array {
    NSMutableArray *itemArray = [NSMutableArray arrayWithCapacity:array.count];
    [array enumerateObjectsUsingBlock:^(NSArray *item, NSUInteger idx, BOOL *stop) {
        if (item.count > 3) {
            [itemArray addObject:[AFDDictionaryItem dictionaryItemWithKey:[item objectAtIndex:1]
                                                                 andText:[item objectAtIndex:0]
                                                          andDescText:[item objectAtIndex:2]
                                                  andIsClearOption:[item objectAtIndex:3]]];
        }else if (item.count > 2) {
            [itemArray addObject:[AFDDictionaryItem dictionaryItemWithKey:[item objectAtIndex:1]
                                                                 andText:[item objectAtIndex:0]
                                                          andDescText:[item objectAtIndex:2]]];
        }else {
            [itemArray addObject:[AFDDictionaryItem dictionaryItemWithKey:[item objectAtIndex:1]
                                                                 andText:[item objectAtIndex:0]]];
        }
    }];
    return itemArray;
}

+ (NSArray<AFDDictionaryItem *> *)genderDictionaryItems {
    return [self dictionaryFromAllArrayFormatFileNamed:@"gender"];
}

+ (NSArray<AFDDictionaryItem*> *)countryDictionaryItems {
    FMDatabaseQueue * queue = [AFDFMDBManager geonameDataBaseQueue];
    NSMutableArray *array = [NSMutableArray array];
    [queue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM country"];
        FMResultSet *resultSet = [db executeQuery:query];
        while ([resultSet next]) {
            int Id = [resultSet intForColumn:@"id"];
            NSString *name = [resultSet stringForColumn:@"name"];
            AFDDictionaryItem *item = [[AFDDictionaryItem alloc] init];
            item.key = [NSString stringWithFormat:@"%d", Id];
            item.text = name;
            [array addObject:item];
        }
        [resultSet close];
    }];
    return array;
}

+ (AFDDictionaryItem *)countryFromPlacemark:(CLPlacemark *)place {
    if (!place){
        return nil;
    }
    FMDatabaseQueue * queue = [AFDFMDBManager geonameDataBaseQueue];
    __block AFDDictionaryItem *item = nil;
    [queue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM country WHERE code = '%@' ORDER BY sn", place.ISOcountryCode];
        FMResultSet *resultSet = [db executeQuery:query];
        while ([resultSet next]) {
            int Id = [resultSet intForColumn:@"id"];
            NSString *name = [resultSet stringForColumn:@"name"];
            item = [[AFDDictionaryItem alloc] init];
            item.key = [NSString stringWithFormat:@"%d", Id];
            item.text = name;
            break;
        }
        [resultSet close];
    }];
    return item;
}

+ (NSArray<AFDDictionaryItem*> *)stateDictionaryOfCountry:(AFDDictionaryItem *)country {
    if (!country) {
        return @[];
    }
    FMDatabaseQueue * queue = [AFDFMDBManager geonameDataBaseQueue];
    NSMutableArray *array = [NSMutableArray array];
    [queue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM state WHERE parent = %@ ORDER BY name", country.key];
        FMResultSet *resultSet = [db executeQuery:query];
        while ([resultSet next]) {
            int Id = [resultSet intForColumn:@"id"];
            NSString *name = [resultSet stringForColumn:@"name"];
            
            AFDDictionaryItem *item = [[AFDDictionaryItem alloc] init];
            item.key = [NSString stringWithFormat:@"%d", Id];
            item.text = name;
            [array addObject:item];
        }
        [resultSet close];
    }];
    return array;
}

+ (AFDDictionaryItem *)stateOfCountry:(AFDDictionaryItem *)country fromPlacemark:(CLPlacemark *)place {
    if (!country || !place) { return nil; }
    FMDatabaseQueue * queue = [AFDFMDBManager geonameDataBaseQueue];
    __block AFDDictionaryItem * targetItem = nil;
    [queue inDatabase:^(FMDatabase *db) {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM state WHERE parent = %@ AND (name = '%@' OR code = '%@')", country.key, place.administrativeArea,place.administrativeArea];
        FMResultSet *resultSet = [db executeQuery:query];
        if ([resultSet next]) {
            int Id = [resultSet intForColumn:@"id"];
            NSString *name = [resultSet stringForColumn:@"name"];
            targetItem = [[AFDDictionaryItem alloc] init];
            targetItem.key = [NSString stringWithFormat:@"%d", Id];
            targetItem.text = name;
        }
        [resultSet close];
    }];
    return targetItem;
}


@end
