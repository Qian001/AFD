//
//  AFDDictionaryItem.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFDDictionaryItem : NSObject

@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSString *descText;
@property (assign, nonatomic) BOOL isClearOption;


+ (id)dictionaryItemWithKey:(NSString *)value andText:(NSString *)text;

+ (id)dictionaryItemWithKey:(NSString *)value andText:(NSString *)text andDescText:(NSString *)desc;

+ (id)dictionaryItemWithKey:(NSString *)value andText:(NSString *)text andDescText:(NSString *)desc andIsClearOption:(BOOL)option;

- (BOOL)isEqualToItem:(AFDDictionaryItem *)item;

@end
