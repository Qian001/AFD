//
//  AFDFMDBManager.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB/FMDatabase.h>
#import <FMDB/FMDatabaseQueue.h>

@interface AFDFMDBManager : NSObject

+ (FMDatabaseQueue *)geonameDataBaseQueue;

@end
