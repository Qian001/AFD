//
//  AFDFMDBManager.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDFMDBManager.h"

@implementation AFDFMDBManager

static FMDatabaseQueue * _geonameDataBaseQueue = nil;

+ (FMDatabaseQueue *)geonameDataBaseQueue {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *path = [[NSBundle mainBundle] pathForResource:@"geoNames" ofType:@"sqlite"];
        if (path == nil) {
            path = [[NSBundle bundleForClass:[self class]] pathForResource:@"geoNames" ofType:@"sqlite"];
        }
        _geonameDataBaseQueue = [FMDatabaseQueue databaseQueueWithPath:path];
    });
    return _geonameDataBaseQueue;
}

@end
