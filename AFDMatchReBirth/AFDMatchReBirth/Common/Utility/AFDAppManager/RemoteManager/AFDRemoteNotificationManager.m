//
//  AFDRemoteNotificationManager.m
//  TFMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDRemoteNotificationManager.h"

@implementation AFDRemoteNotificationManager

+ (instancetype)sharedManager {
    static AFDRemoteNotificationManager *notificationManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        notificationManager = [[AFDRemoteNotificationManager alloc] init];
    });
    
    return notificationManager;
}

-(id)init {
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setApplicationBadgeCount) name:NotificationRefreshNoticeDidUpdated object:nil];
    }
    return self;
}

-(void)registerNotificationSettings {
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        //iOS 10 unique
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert | UNAuthorizationOptionBadge | UNAuthorizationOptionSound) completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (granted) {
                NSLog(@"Register successful!");
                [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
                    NSLog(@"%@", settings);
                }];
            } else {
                NSLog(@"Register failure!");
            }
        }];
    }else{
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeSound | UIUserNotificationTypeBadge | UIUserNotificationTypeAlert categories:nil] ];
    }
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

/*
 *  iOS 10 Notification bar go to App Delegate
 */
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    [AFDRemoteNotificationManager processNotification:userInfo fetchCompletionHandler:completionHandler];
    // Warning: UNUserNotificationCenter delegate received call to -userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: but the completion handler was never called.
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateActive){
        completionHandler();  // System requirements method of execution    }
    }
}

// iOS 10 received notify
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    if ([AFDSessionManager shareManager].session) {
//        MSReqRefreshNotice *request = [[MSReqRefreshNotice alloc] init];
//        [request request:^(NSURLSessionDataTask *task, id responseObject) {
//
//        } failure:^(NSURLSessionDataTask *task, NSError *error) {
//
//        }];
    }
    completionHandler(UNNotificationPresentationOptionBadge|
                      UNNotificationPresentationOptionSound|
                      UNNotificationPresentationOptionAlert);
}

-(void)setDeviceToken:(NSString *)deviceToken {
    _deviceToken = deviceToken;
    [self updateToken];
}

-(void)updateToken {
    if (self.deviceToken && [AFDSessionManager shareManager].session) {
//        MSReqUpdateToken *request = [[MSReqUpdateToken alloc] init];
//        [request  request:^(NSURLSessionDataTask *task, id responseObject) {
//
//        } failure:^(NSURLSessionDataTask *task, NSError *error) {
//
//        }];
    }
}

+(void)processNotification:(NSDictionary *)dictionary fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    AFDRemoteNotificationModel *model = [[AFDRemoteNotificationModel alloc] initWithRemoteInfo:dictionary];
    [AFDRemoteNotificationManager sharedManager].launchRemoteNotification = model;
    [[NSNotificationCenter defaultCenter] postNotificationName:TFNotificationLaunchFromRemoteNotification object:nil];
    
//    if (model.type == MSNotificationTypeVisitor) {
//        [[MSProfileManager sharedProfileManager].profile.visitorNotice increaseNewNumberWithoutNoticication];
//    } else if (model.type == MSNotificationTypeIntrestedInMe) {
//        [[MSProfileManager sharedProfileManager].profile.favoritedNotice increaseNewNumberWithoutNoticication];
//    } else if (model.type == MSNotificationTypeRequestYourPrivatePhotos) {
//        [MSProfileManager sharedProfileManager].profile.privateAlbumRequestNewNum++;
//        [MSProfileManager sharedProfileManager].profile.privateAlbumRequestTotalNum++;
//    } else if (model.type == MSNotificationTypeWink) {
    
//    } else if (model.type == MSNotificationTypeMessage) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:MSNotificationMyProfileCommentsChanged object:nil];
//    }
//    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationRefreshNoticeDidUpdated object:nil];
//    [[NSNotificationCenter defaultCenter] postNotificationName:MSNotificationConnectionBeginRefresh object:nil];
//    [[MSProfileManager sharedProfileManager] storeProfile];
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        if ([AFDSessionManager shareManager].session) {
//            MSReqRefreshNotice *request = [[MSReqRefreshNotice alloc] init];
//            [request request:^(NSURLSessionDataTask *task, id responseObject) {
//
//            } failure:^(NSURLSessionDataTask *task, NSError *error) {
//
//            }];
        }
    }
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)setApplicationBadgeCount {
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(numberOfApplicationIconBadgeInRemoteNotificationManager:)]) {
        NSInteger count = [self.dataSource numberOfApplicationIconBadgeInRemoteNotificationManager:self];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:count];
        return;
    }
//    MSMyProfileModel *profile = [MSProfileManager sharedProfileManager].profile;
//    __block NSInteger count = profile.visitorNotice.newNum + profile.favoritedNotice.newNum + profile.activityAndBlogNotice.newNum + profile.letsMeetNotice.newNum + profile.sparkLikeMeNotice.newNum + profile.privateAlbumRequestNewNum;
//
//    if (APP_OPTIONS.enableGift) {
//        count += profile.receivedGiftsNumber;
//    }
//
//    if (profile.account.userId) {
//        if ([MSAppInfoManager shouldUseIMChat]) {
//            [[MSXMPPServiceCoreDataManager shareManager] queryNumberOfUnreadMessagesWithCompletionHandler:^(NSUInteger unreadCount) {
//                count += unreadCount;
//                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:count];
//            }];
//        } else {
//            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:count];
//        }
//    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
