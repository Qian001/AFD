//
//  AFDRemoteNotificationManager.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

@class AFDRemoteNotificationManager;

@protocol AFDRemoteNotificationManagerDataSource <NSObject>

- (NSInteger)numberOfApplicationIconBadgeInRemoteNotificationManager:(AFDRemoteNotificationManager *)manager;

@end

@interface AFDRemoteNotificationManager: NSObject <UNUserNotificationCenterDelegate>

@property (nonatomic, strong) NSString *deviceToken;
@property (nonatomic, strong) AFDRemoteNotificationModel *launchRemoteNotification;
@property (nonatomic, weak) id <AFDRemoteNotificationManagerDataSource>dataSource;

-(void)registerNotificationSettings;

+ (instancetype)sharedManager;

+(void)processNotification:(NSDictionary *)dictionary fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;

-(void)updateToken;

@end

