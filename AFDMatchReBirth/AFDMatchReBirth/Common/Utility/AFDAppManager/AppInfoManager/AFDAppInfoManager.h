//
//  AFDAppInfoManager.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, AFDAppType) {
    AFDAppTypeiOS = 1, // iOS
    AFDAppTypeAndroid = 2 // android
};

typedef NS_ENUM(NSUInteger, AFDAppPlatform) {
    AFDAppPlatformOldiOS = 3,
    AFDAppPlatformLite = 6,
    AFDAppPlatformPro = 7
};

@interface AFDAppInfoManager : NSObject


+ (NSString *)appDisplayName;

+ (NSString *)bundleIdentifier;

+ (NSString *)appVersion;

+ (NSString *)buildVersion;

+ (NSDictionary *)dictionaryForCustomFields;

+ (NSString *)appShortDisplayName;

+ (NSString *)appLongDisplayName;

+ (AFDAppType)appType;

+ (NSString *)appPlatform;

/**
 * Don't use
 */
+ (NSString *)appTid;

+ (NSString *)appID;

+ (NSString *)appBranchName;

+ (NSString *)branchNumber;

+ (NSURL *)appStoreURL;

+ (NSURL *)appStoreRateUsURL;

+ (NSString *)apiVersion;

+ (NSDictionary *)hostListDictionary;

+ (NSString *)hostKeyInInfoPlist;

+ (NSString *)host;

+ (NSString *)webSiteHomePage;

+ (NSString *)serviceAgreement;

+ (NSString *)privacyPolicy;

+ (NSString *)emailAddressForSupport;

@end
