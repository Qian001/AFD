//
//  AFDAppInfoManager.m
//  TFMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDAppInfoManager.h"

@implementation AFDAppInfoManager

+ (NSString *)appDisplayName {
    NSString *result = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
    return result;
}

+ (NSString *)bundleIdentifier {
    NSString *bundleIdentifier = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
    return bundleIdentifier;
}

+ (NSString *)appVersion {
    NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    return appVersion;
}

+ (NSString *)buildVersion {
    NSString *buildVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    return buildVersion;
}


+ (NSDictionary *)dictionaryForCustomFields {
    static NSDictionary *dictionaryForCustomFields;
    if (!dictionaryForCustomFields) {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Config.plist" ofType:nil];
        dictionaryForCustomFields = [NSDictionary dictionaryWithContentsOfFile:filePath][@"TFCustomFields"];
    }
    return dictionaryForCustomFields;
}

+ (NSString *)appShortDisplayName {
    NSString *result = [[self dictionaryForCustomFields] objectForKey:@"TFShortDisplayName"];
    if (!result) {
        result = [self appDisplayName];
    }
    return result;
}

+ (NSString *)appLongDisplayName {
    NSString *result = [[self dictionaryForCustomFields] objectForKey:@"TFLongDisplayName"];
    if (!result) {
        result = [self appDisplayName];
    }
    return result;
}

+ (AFDAppType)appType {
    return AFDAppTypeiOS;
}

/**
 * Don't use
 */

+ (NSString *)appPlatform {
    BOOL isProPlatform = [[[self dictionaryForCustomFields] objectForKey:@"TFAppIeProPlatform"] boolValue];
    NSString *platform = @(AFDAppPlatformLite).description;
    if (isProPlatform) {
        platform = @(AFDAppPlatformPro).description;
    }
    return platform;
}

+ (NSString *)appTid {
    NSString *appTid = [[self dictionaryForCustomFields] objectForKey:@"TFTid"];
    return appTid;
}

+ (NSString *)appID {
    NSString *appId = [[[self class] dictionaryForCustomFields] objectForKey:@"TFAppID"];
    return appId;
}

+ (NSString *)appBranchName {
    NSString *name = [[self dictionaryForCustomFields] objectForKey:@"TFBranchName"];
    return name;
}

+ (NSString *)branchNumber {
    NSString *branch;
    NSArray *results = [[self buildVersion] componentsSeparatedByString:@"."];
    if (results.count >= 2) {
        branch = results[1];
    }
    return branch;
}

+ (NSURL *)appStoreURL {
    NSString *link = [NSString stringWithFormat:@"https://itunes.apple.com/us/app/id%@?ls=1&mt=8", [self appID]];
    NSURL *appStoreURL = [NSURL URLWithString:link];
    return appStoreURL;
}

+ (NSURL *)appStoreRateUsURL {
    return [NSURL URLWithString:[NSString stringWithFormat:@"https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8", [self appID]]];
}

+ (NSString *)apiVersion {
    NSString *apiVersion = [[[self class] dictionaryForCustomFields] objectForKey:@"MSAPIVersion"];
    return apiVersion;
}

+ (NSDictionary *)hostListDictionary {
    static NSDictionary *hostList = nil;
    if (hostList == nil) hostList = [[self dictionaryForCustomFields] objectForKey:@"TFHostList"];
    return hostList;
}

+ (NSString *)hostKeyInInfoPlist {
    NSString *key = [[self dictionaryForCustomFields] objectForKey:@"TFHostKeyInPlist"];
    return key;
}

+ (NSString *)host {
    NSString *key = [[self class] hostKeyInInfoPlist];
    NSDictionary *dict = [[self class] hostListDictionary];
    NSString *host = [dict objectForKey:key];
    return [NSString stringWithFormat:@"%@", host];
}

+ (NSString *)webSiteHomePage {
    NSString *link = [[self dictionaryForCustomFields] objectForKey:@"TFWebSiteHomePage"];
    return link;
}

+ (NSString *)serviceAgreement {
    NSString *link = [[self dictionaryForCustomFields] objectForKey:@"TFServiceAgreement"];
    return link;
}

+ (NSString *)privacyPolicy {
    NSString *link = [[self dictionaryForCustomFields] objectForKey:@"TFPrivacyPolicy"];
    return link;
}

+ (NSString *)emailAddressForSupport {
    NSString *resource = [[self dictionaryForCustomFields] objectForKey:@"TFSupportEmail"];
    return resource;
}

@end
