//
//  AFDDIYFooter.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDDIYFooter.h"

@interface AFDDIYFooter()

@property (weak, nonatomic) UIActivityIndicatorView *loading;
@property (weak, nonatomic) UIView *bottomView;

@end

@implementation AFDDIYFooter

- (void)prepare {
    [super prepare];
    // Sets the height of the control
    self.mj_h = 70;
    UIView *bottomView = [[UIView alloc] init];
    [self addSubview:bottomView];
    self.bottomView = bottomView;
    self.bottomView.hidden = YES;
    UILabel * middleLab = [[UILabel alloc] init];
    middleLab.text = @"~ No more data ~";
    middleLab.font = [UIFont systemFontOfSize:13];
    middleLab.textAlignment = NSTextAlignmentCenter;
    middleLab.textColor = APP.lightTextColor;
    [self.bottomView addSubview:middleLab];
    [middleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(0);
        make.height.mas_equalTo(@(17));
    }];
    
    UILabel * leftLine = [[UILabel alloc] init];
    leftLine.hidden = YES;
    leftLine.backgroundColor = APP.lightTextColor;
    [self.bottomView addSubview:leftLine];
    [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.right.mas_equalTo(middleLab.mas_left).offset(-16);
        make.size.mas_equalTo(CGSizeMake(54, 0.5));
    }];
    
    UILabel * rightLine = [[UILabel alloc] init];
    rightLine.backgroundColor = APP.lightTextColor;
    rightLine.hidden = YES;
    [self.bottomView addSubview:rightLine];
    [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(middleLab.mas_right).offset(16);
        make.size.mas_equalTo(CGSizeMake(54, 0.5));
    }];
    
    // loading
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self addSubview:loading];
    self.loading = loading;
}

#pragma mark - Set the location and size of the child control here

- (void)placeSubviews {
    [super placeSubviews];
    
    self.bottomView.frame = self.bounds;
    
    self.loading.center = CGPointMake(self.bounds.size.width / 2, self.mj_h * 0.5);
}

#pragma mark - Listen for scrollView's contentOffset changes

- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change {
    [super scrollViewContentOffsetDidChange:change];
    
}

#pragma mark - Listen for scrollView's contentSize change

- (void)scrollViewContentSizeDidChange:(NSDictionary *)change {
    [super scrollViewContentSizeDidChange:change];
}

#pragma mark - Listen for drag status changes for scrollView

- (void)scrollViewPanStateDidChange:(NSDictionary *)change {
    [super scrollViewPanStateDidChange:change];
}

#pragma mark - Listens for the refresh state of the control

- (void)setState:(MJRefreshState)state {
    MJRefreshCheckState;
    switch (state) {
        case MJRefreshStateIdle:
            self.bottomView.hidden = YES;
            [self.loading stopAnimating];
            break;
        case MJRefreshStateRefreshing:
            self.bottomView.hidden = YES;
            [self.loading startAnimating];
            break;
        case MJRefreshStateNoMoreData:
            self.bottomView.hidden = NO;
            [self.loading stopAnimating];
            break;
        default:
            break;
    }
}

@end
