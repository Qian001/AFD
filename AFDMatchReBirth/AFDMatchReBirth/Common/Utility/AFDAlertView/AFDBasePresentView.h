//
//  AFDBasePresentView.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DismissCompletion)(void);

@interface AFDBasePresentView : UIView<UIGestureRecognizerDelegate>

@property (nonatomic, assign) BOOL isTapToDismiss;

@property (nonatomic, copy) DismissCompletion dismissCompeltion;

@property (nonatomic, strong, readonly) UIView *contentView;

- (void)presentAnimated:(BOOL)flag completion:(void (^)(void))completion;

- (void)presentAnimated:(BOOL)flag animation:(void(^)(void))animation completion:(void (^)(void))completion;

- (void)dismissAnimated:(BOOL)flag completion:(void (^)(void))completion;

- (void)showAnimated:(BOOL)flag animation:(void(^)(void))animation completion:(void (^)(void))completion;

- (void)hideAnimated:(BOOL)flag animation:(void(^)(void))animation completion:(void (^)(void))completion;

@end
