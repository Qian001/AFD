//
//  AFDBasePresentView.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDBasePresentView.h"

@interface AFDBasePresentView ()

@property (nonatomic, strong) UITapGestureRecognizer *tapToDismissGestures;
@property (nonatomic, strong) UIView *glassColorView;

@end

@implementation AFDBasePresentView

- (id)init {
    self = [self initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
    if (self) {
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initializator];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initializator];
    }
    return self;
}

- (void)initializator {
    _glassColorView = [[UIView alloc] initWithFrame:CGRectZero];
    self.glassColorView.alpha = 0.0f;
    self.glassColorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    [self addSubview:self.glassColorView];
    
    _contentView = [[UIView alloc] initWithFrame:CGRectZero];
    [self addSubview:self.contentView];
    
    self.tapToDismissGestures = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGestures:)];
    self.tapToDismissGestures.delegate = self;
    [self addGestureRecognizer:self.tapToDismissGestures];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.glassColorView.frame = self.bounds;
    self.contentView.frame = [self contentFrame];
}

- (CGRect)contentFrame {
    CGFloat selfHeight = self.frame.size.height;
    CGFloat selfWidth = self.frame.size.width;
    
    CGFloat contentWidth = 200;
    CGFloat contentHeight = 200;
    CGRect frame = CGRectMake((selfWidth - contentWidth)/2,
                              (selfHeight - contentHeight)/2,
                              contentWidth,
                              contentHeight);
    return frame;
}

- (void)preparePresent {
    UIViewController *presentedViewController = [UIViewController topMostController];
    UIView *superView = presentedViewController.view;
    
    if ([[superView window] subviews].lastObject != superView) {
        superView = presentedViewController.view.window;
    }
    [superView addSubview:self];
    self.frame = superView.bounds;
    
    [self.contentView moveOriginToPointY:self.frame.size.height];
}

- (void)presentAnimated:(BOOL)flag completion:(void (^)(void))completion {
    [self presentAnimated:flag animation:nil completion:completion];
}

- (void)presentAnimated:(BOOL)flag animation:(void(^)(void))animation completion:(void (^)(void))completion {
    [self preparePresent];
    UIViewController *topMostController = [UIViewController topMostController];
    UIView *superView = topMostController.view;
    if (flag) {
        superView.userInteractionEnabled = NO;
        if (animation)  {
            self.glassColorView.alpha = 0.0;
            [UIView animateWithDuration:0.25 animations:^{
                animation();
                self.glassColorView.alpha = 1.0;
            } completion:^(BOOL finished) {
                if (completion) completion();
                superView.userInteractionEnabled = YES;
            }];
        } else {
            self.contentView.frame = [self contentFrame];
            [self.contentView layoutIfNeeded];
            [self.contentView moveOriginToPointY:CGRectGetHeight(self.frame)];
            [UIView animateWithDuration:0.25 animations:^{
                self.glassColorView.alpha = 1.0;
                [self.contentView moveOriginToPointY:CGRectGetMinY([self contentFrame])];
            } completion:^(BOOL finished) {
                if (completion) completion();
                superView.userInteractionEnabled = YES;
            }];
        }
    } else {
        self.glassColorView.alpha = 1.0;
        self.contentView.frame = [self contentFrame];
        if (completion) completion();
    }
}

- (void)dismissAnimated:(BOOL)flag completion:(void (^)(void))completion {
    [self dismissAnimated:flag animation:nil completion:completion];
}

- (void)dismissAnimated:(BOOL)flag animation:(void(^)(void))animation completion:(void (^)(void))completion {
    if (flag) {
        if (animation) {
            [UIView animateWithDuration:0.25 animations:^{
                animation();
            } completion:^(BOOL finished) {
                if (completion) completion();
                [self removeFromSuperview];
                
                if (self.dismissCompeltion){
                    self.dismissCompeltion();
                    self.dismissCompeltion = nil;
                }
            }];
        } else {
            [UIView animateWithDuration:0.25 animations:^{
                self.glassColorView.alpha = 0.0;
                [self.contentView moveOriginToPointY:self.frame.size.height];
            } completion:^(BOOL finished) {
                if (completion) completion();
                [self removeFromSuperview];
                
                if(self.dismissCompeltion){
                    self.dismissCompeltion();
                    self.dismissCompeltion = nil;
                }
            }];
        }
    } else {
        if (completion) completion();
        [self removeFromSuperview];
        
        if (self.dismissCompeltion) {
            self.dismissCompeltion();
            self.dismissCompeltion = nil;
        }
    }
}

- (void)showAnimated:(BOOL)flag animation:(void(^)(void))animation completion:(void (^)(void))completion {
    if (flag) {
        self.backgroundColor = [UIColor clearColor];
        [UIView animateWithDuration:0.25 animations:^{
            self.glassColorView.alpha = 1.0;
            [self.contentView moveOriginToPointY:(self.frame.size.height - self.contentView.frame.size.height)];
            if (animation) animation();
        } completion:^(BOOL finished) {
            if (completion) completion();
        }];
    } else {
        self.glassColorView.alpha = 1.0;
        [self.contentView moveOriginToPointY:(self.frame.size.height - self.contentView.frame.size.height)/2];
        if (completion) completion();
    }
}


- (void)hideAnimated:(BOOL)flag animation:(void(^)(void))animation completion:(void (^)(void))completion {
    if (flag) {
        [UIView animateWithDuration:0.25 animations:^{
            self.glassColorView.alpha = 0.0;
            [self.contentView moveOriginToPointY:self.frame.size.height];
            if (animation) animation();
        } completion:^(BOOL finished) {
            if (completion) completion();
        }];
    } else {
        if (completion) completion();
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (self.tapToDismissGestures == gestureRecognizer) {
        CGPoint point = [gestureRecognizer locationInView:self.contentView];
        BOOL contains = CGRectContainsPoint(self.contentView.bounds, point) == NO;
        if (contains && self.isTapToDismiss) {
            return YES;
        } else {
            return NO;
        }
    }
    return YES;
}

- (void)handleGestures:(UIGestureRecognizer *)gestures {
    if (gestures == self.tapToDismissGestures) {
        [self dismissAnimated:YES completion:nil];
    }
}

@end
