//
//  AFDPresentAlertView.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDPresentAlertView.h"

@implementation AFDAlertButtonItem

+ (AFDAlertButtonItem *)buttonItemWithTitle:(NSString *)title action:(void (^)(void))action bold:(BOOL)isBold {
    AFDAlertButtonItem * item = [[AFDAlertButtonItem alloc] init];
    item.title = title;
    item.action = action;
    item.boldItem = isBold;
    return item;
}

- (UIButton *)createButton {
    UIButton * button = [[UIButton alloc] init];
    button.titleLabel.font = _boldItem ? [UIFont appBoldFontOfSize:16] : [UIFont appFontOfSize:16];
    [button setTitleColor:_redItem ? [UIColor colorWithHexRGB:@"ee493f"] : [APP appMainColor] forState:UIControlStateNormal];
    [button setTitle:self.title forState:UIControlStateNormal];
    [button setTitleColor:[APP lightTextColor] forState:UIControlStateDisabled];
    button.enabled = !_disabled;
    return button;
}

@end

@interface AFDPresentAlertView()

@property (nonatomic, strong) NSArray<AFDAlertButtonItem *> * items;

@end

@implementation AFDPresentAlertView

- (instancetype)initWithButtonItems:(NSArray<AFDAlertButtonItem *> *)items {
    self = [super init];
    if (self) {
        self.isTapToDismiss = YES;
        _items = items;
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:.2];
    
    self.contentView.layer.cornerRadius = 4;
    self.contentView.layer.masksToBounds = YES;
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    NSMutableArray * temp = [@[] mutableCopy];
    NSMutableArray * temp2 = [@[] mutableCopy];
    [_items enumerateObjectsUsingBlock:^(AFDAlertButtonItem * obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton * button = [obj createButton];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:button];
        [temp addObject:button];
        
        UIView * separator = [[UIView alloc] init];
        separator.backgroundColor = APP.tableCellSeparationColor;
        [self.contentView addSubview:separator];
        [temp2 addObject:separator];
        
    }];
    _buttons = [temp copy];
    _separators = [temp2 copy];
    
    _titleBar = [[UIView alloc] init];
    _titleBar.backgroundColor = [APP appMainColor];
    [self.contentView addSubview:_titleBar];
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.numberOfLines = 0;
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.font = [UIFont appBoldFontOfSize:17];
    [self.contentView addSubview:_titleLabel];
    
    _textLabel = [[UILabel alloc] init];
    _textLabel.numberOfLines = 0;
    _textLabel.textAlignment = NSTextAlignmentCenter;
    _textLabel.textColor = APP.lightTextColor;
    _textLabel.font = [UIFont appBoldFontOfSize:15];
    [self.contentView addSubview:_textLabel];
    
    _imageView = [[UIImageView alloc] init];
    [self.contentView addSubview:_imageView];
}

- (void)layoutSubviews {
    if ((_titleLabel.text.length && !_textLabel.text.length && !_imageView.image) || _showImageViewAtTop) {
        _titleLabel.textColor = APP.lightTextColor;
        _titleBar.backgroundColor = [UIColor clearColor];
    }
    
    CGFloat contentWidth = 280;
    if (SCREEN_WIDTH > 320) {
        contentWidth = 310;
    }
    self.contentView.width = contentWidth;
    
    __block CGFloat y = 25;
    CGFloat pan = (_textLabel.text.length > 0 || _imageView.image) ? 25 : 0;
    if (_titleLabel.text.length) {
        _titleLabel.frame = CGRectMake(25, 17, contentWidth - 50, [_titleLabel sizeThatFits:CGSizeMake(contentWidth - 50, MAXFLOAT)].height);
        _titleBar.frame = CGRectMake(0, 0, contentWidth, _titleLabel.maxY + 17);
        y = _titleBar.maxY + pan;
    }
    
    if (_imageView.image) {
        [_imageView sizeToFit];
        _imageView.center = CGPointMake(contentWidth/2.f, y + _imageView.height/2.f);
        y = _imageView.maxY + pan;
    }
    
    if (_textLabel.text.length) {
        NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:5];
        paragraphStyle.alignment = NSTextAlignmentCenter;
        NSMutableAttributedString * ats = [[NSMutableAttributedString alloc] initWithString:_textLabel.text attributes:@{NSParagraphStyleAttributeName: paragraphStyle}];
        _textLabel.attributedText = ats;
        if (self.textShouldOneLine == YES) {
            _textLabel.numberOfLines = 1;
            _textLabel.frame = CGRectMake(25, y, contentWidth - 50, 20);
            [_textLabel sizeWithLimitedWidth:contentWidth - 50];
        } else {
            _textLabel.frame = CGRectMake(25, y, contentWidth - 50, [_textLabel sizeThatFits:CGSizeMake(contentWidth - 50, MAXFLOAT)].height);
        }
        y = _textLabel.maxY + pan;
    }
    
    [_buttons enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIView * separator = self.separators[idx];
        separator.frame = CGRectMake(0, y, contentWidth, 0.5);
        obj.frame = CGRectMake(0, y, contentWidth, 48);
        y = obj.maxY;
    }];
    if (_buttons.count == 2) {
        UIButton * buttonLeft = _buttons[0];
        UIButton * buttonRight = _buttons[1];
        
        buttonLeft.width =
        buttonRight.width = contentWidth/2.f;
        
        buttonRight.y = buttonLeft.y;
        buttonRight.x = contentWidth/2.f;
        
        UIView * separator = _separators[1];
        separator.frame = CGRectMake(contentWidth/2.f, buttonLeft.y, 0.5, buttonLeft.height);
        y = buttonLeft.maxY;
    }
    self.contentView.height = y;
    self.contentView.center = self.boundsCenter;
}

- (void)buttonAction:(UIButton *)sender {
    AFDAlertButtonItem * item = _items[[_buttons indexOfObject:sender]];
    if (item.action) {
        item.action();
    }
    [self dismissAnimated:YES completion:NULL];
}

@end
