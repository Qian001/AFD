//
//  AFDPresentAlertView.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDBasePresentView.h"

@interface AFDAlertButtonItem : NSObject

@property (nonatomic, assign) BOOL boldItem;

@property (nonatomic, assign) BOOL redItem;

@property (nonatomic, assign) BOOL disabled;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, copy) void(^action)(void);

+ (AFDAlertButtonItem *)buttonItemWithTitle:(NSString *)title action:(void(^)(void))action bold:(BOOL)isBold;

- (UIButton *)createButton;

@end;

@interface AFDPresentAlertView : AFDBasePresentView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, assign) BOOL textShouldOneLine;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) UIImageView * imageView;
@property (nonatomic, assign) BOOL showImageViewAtTop;
@property (nonatomic, strong) UIView *titleBar;
@property (nonatomic, strong) NSArray<UIButton *> *buttons;
@property (nonatomic, strong) NSArray<UIView *> *separators;
- (instancetype)initWithButtonItems:(NSArray<AFDAlertButtonItem *> *)items;

@end
