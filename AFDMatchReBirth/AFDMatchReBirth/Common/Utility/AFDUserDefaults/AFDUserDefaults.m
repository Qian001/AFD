//
//  AFDUserDefaults.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDUserDefaults.h"

@implementation AFDUserDefaults

+ (void)saveUserDefaults:(NSString *)key value:(id)value {
    if (!key) return;
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (id)userDefaultsBy:(NSString *)key {
    return [[NSUserDefaults standardUserDefaults] valueForKey:key];
}

+ (void)removeValueBy:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)saveObject:(id)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:encodedObject forKey:key];
    [defaults synchronize];
}

+ (id)loadObjectWithKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    id object = nil;
    if(encodedObject){
        @try {
            object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
            return object;
        } @catch (NSException *exception) {
            return object;
        } @finally {
            
        }
    }
    return object;
}

@end
