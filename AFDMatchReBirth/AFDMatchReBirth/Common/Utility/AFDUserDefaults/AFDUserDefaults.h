//
//  AFDUserDefaults.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFDUserDefaults : NSObject

+ (void)saveUserDefaults:(NSString *)key value:(id)value;

+ (id)userDefaultsBy:(NSString *)key;

+ (void)removeValueBy:(NSString *)key;

+ (void)saveObject:(id)object key:(NSString *)key;

+ (id)loadObjectWithKey:(NSString *)key;

@end
