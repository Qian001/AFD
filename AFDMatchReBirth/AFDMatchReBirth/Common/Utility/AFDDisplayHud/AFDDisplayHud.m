//
//  AFDDisplayHud.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDDisplayHud.h"

@interface AFDDisplayHud ()

@property (nonatomic, strong) UIView *currentToastViewLabel;
@property (nonatomic, strong) NSString *oldErrorMsg;

@end

@implementation AFDDisplayHud

+(AFDDisplayHud *)shareInstance {
    static dispatch_once_t onceToken;
    static AFDDisplayHud *_instance;
    dispatch_once(&onceToken, ^{
        _instance = [[AFDDisplayHud alloc] init];
    });
    return _instance;
}

- (void)displayMessagesDismissDelayed:(NSString *)message {
    if ([self.oldErrorMsg isEqualToString:message]) {
        return;
    }
    [self showToast:message aBackgroundColor:[UIColor colorWithHexRGB:@"#E56060"] aShowTime:2 dismiss:^{
        self.oldErrorMsg = nil;
    }];
    self.oldErrorMsg = message;
}

- (void)displayErrorMessageDismissDelayed:(NSString *)message {
    if ([self.oldErrorMsg isEqualToString:message]) {
        return;
    }
    [self showToast:message aBackgroundColor:[UIColor colorWithHexRGB:@"#1ec671"] aShowTime:12 dismiss:^{
        self.oldErrorMsg = nil;
    }];
    self.oldErrorMsg = message;
}

- (void)displayNoticesDismissDelayed:(NSString *)notice {
    if ([self.oldErrorMsg isEqualToString:notice]) {
        return;
    }
    [self showToast:notice aBackgroundColor:[UIColor colorWithHexRGB:@"#b5b5b5"] aShowTime:2 dismiss:^{
        self.oldErrorMsg = nil;
    }];
    self.oldErrorMsg = notice;
}

- (void)showToast:(NSString *)message aBackgroundColor:(UIColor*)aBackgroundColor aShowTime:(NSTimeInterval)aShowTime dismiss:(void (^)(void)) dismiss {
    UIView *toastViewLabel = self.currentToastViewLabel;
    [toastViewLabel removeFromSuperview];
    toastViewLabel.backgroundColor = aBackgroundColor;
    id<UIApplicationDelegate> AppDlgt = [UIApplication sharedApplication].delegate;
    UIView *window = nil;
    if ([UIApplication sharedApplication].windows.lastObject) {
        window = [UIApplication sharedApplication].windows.lastObject;
    }else {
        window = AppDlgt.window;
    }
    [window addSubview:toastViewLabel];
    [window bringSubviewToFront:toastViewLabel];
    
    UILabel *aLabel = [toastViewLabel viewWithTag:100];
    aLabel.text = message;
    
    CGFloat height = [NSString calculateForString:message font:aLabel.font maxWidth:SCREEN_WIDTH - 32].height + 32;
    height  = height < 64 ? 64 : height;
    height = IS_IPHONE_X ? height + 24 : height;
    toastViewLabel.frame = CGRectMake(0, - height, SCREEN_WIDTH, height);
    
    CGFloat top = IS_IPHONE_X ? 24 : 20;
    aLabel.frame = CGRectMake(16, top - 4, SCREEN_WIDTH - 32, height - top);
    
    [UIView animateWithDuration:0.25 animations:^{
        toastViewLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH, height);
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(aShowTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.15 animations:^{
             toastViewLabel.frame = CGRectMake(0, - height, SCREEN_WIDTH, height);
        }completion:^(BOOL finished) {
            [toastViewLabel removeFromSuperview];
            if (dismiss) {
                dismiss();
            }
        }];
    });
}

- (void)dismiss {
    CGRect rec = self.currentToastViewLabel.frame;
    [UIView animateWithDuration:0.15 animations:^{
        self.currentToastViewLabel.frame = CGRectMake(0, - rec.size.height, SCREEN_WIDTH, rec.size.height);
    }completion:^(BOOL finished) {
        [self.currentToastViewLabel removeFromSuperview];
        self.oldErrorMsg = nil;
    }];
}

- (void)panGestureRecognizer:(UIPanGestureRecognizer *)panGestureRecognizer{
    CGPoint point = [panGestureRecognizer translationInView:panGestureRecognizer.view.superview];
    if (point.y < -5) {
        [self dismiss];
    }
}

- (UIView *)currentToastViewLabel {
    if (!_currentToastViewLabel) {
        UIView *toastViewLabel = [[UIView alloc]init];
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognizer:)];
        [toastViewLabel addGestureRecognizer:pan];
        UILabel *aLabel = [[UILabel alloc]init];
        aLabel.tag = 100;
        aLabel.font = [UIFont appFontOfSize:16];
        aLabel.textColor = [UIColor whiteColor];
        aLabel.lineBreakMode = NSLineBreakByCharWrapping;
        aLabel.backgroundColor = [UIColor clearColor];
        aLabel.textAlignment = NSTextAlignmentCenter;
        aLabel.numberOfLines = 0;
        [toastViewLabel addSubview:aLabel];
        _currentToastViewLabel = toastViewLabel;
    }
    return _currentToastViewLabel;
}

@end
