//
//  AFDDisplayHud.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFDDisplayHud : NSObject

+ (AFDDisplayHud *)shareInstance;

/**
 * Successful Message
 */
- (void)displayMessagesDismissDelayed:(NSString *)message;

/**
 * Error Message
 */
- (void)displayErrorMessageDismissDelayed:(NSString *)message;

/**
 * Notification Message
 */
- (void)displayNoticesDismissDelayed:(NSString *)notice;

@end
