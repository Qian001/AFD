//
//  AFDLocationManager.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDLocationManager.h"
#import "CLLocation+Parsing.h"

typedef void(^LocationSuccess)(CLLocation *location);
typedef void(^LocationFailure)(NSError *error);

@interface AFDLocationManager()

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, copy) LocationSuccess locationSuccess;
@property (nonatomic, copy) LocationFailure locationFailure;

@end

@implementation AFDLocationManager

- (instancetype)init {
    if (self = [super init]) {
        self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
        self.locationManager.distanceFilter = 1000;
        self.locationManager.delegate = self;
    }
    return self;
}

- (void)startLocate:(void(^)(CLLocation *location))success failure:(void(^)(NSError *error))failure {
    self.locationSuccess = success;
    self.locationFailure = failure;
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
}

- (void)startLocateGeocodeParser:(void(^)(CLLocation *location, AFDDictionaryItem *country, AFDDictionaryItem *state, AFDDictionaryItem *city, NSString *zip))success locateFailure:(void(^)(NSError *error))locateFailure geocodeFailure:(void(^)(CLLocation *location, NSError *error))geocodeFailure {
    [self startLocate:^(CLLocation *location) {
        [location geocodeParser:success failure:geocodeFailure];
        [self cancelLocate];
    } failure:locateFailure];
}

- (BOOL)authorizationAuthorized {
    return [CLLocationManager authorizationStatus] != kCLAuthorizationStatusNotDetermined &&
    [CLLocationManager authorizationStatus] != kCLAuthorizationStatusRestricted &&
    [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied;
}

- (void)cancelLocate {
    [self.locationManager stopUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    _location = [locations lastObject];
    if (self.locationSuccess) {
        self.locationSuccess(_location);
//        self.locationSuccess = nil;
    }
    [self cancelLocate];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    if (self.locationFailure) {
        self.locationFailure(error);
    }
    [self cancelLocate];
}

#pragma mark - Getters

- (CLLocationManager *)locationManager {
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
    }
    return _locationManager;
}

@end
