//
//  AFDLocationManager.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface AFDLocationManager : NSObject<CLLocationManagerDelegate>

- (void)startLocateGeocodeParser:(void(^)(CLLocation *location, AFDDictionaryItem *country, AFDDictionaryItem *state, AFDDictionaryItem *city, NSString *zip))success
                   locateFailure:(void(^)(NSError *error))locateFailure
                  geocodeFailure:(void(^)(CLLocation *location,NSError *error))geocodeFailure;

- (BOOL)authorizationAuthorized;

@end
