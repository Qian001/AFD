//
//  AFDNoResultView.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFDNoResultView : UIView

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *textlabel;
@property (nonatomic, strong) UIButton *editButton;

- (void)commonInit;

@end
