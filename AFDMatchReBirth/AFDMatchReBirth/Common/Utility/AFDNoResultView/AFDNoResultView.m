//
//  TFNoResultView.m
//  TFMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDNoResultView.h"

@interface AFDNoResultView()

@property (nonatomic, strong) UIView *borderView;
@property (nonatomic, strong) CAShapeLayer *shapeLayer;

@end

@implementation AFDNoResultView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = APP.defaultTextColor;
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.font = [UIFont appBoldFontOfSize:17];
        _titleLabel.numberOfLines = 0;
        _titleLabel.text = @"NO profiles found";
        [self.borderView addSubview:_titleLabel];
    }
    return _titleLabel;
}

- (UIButton *)editButton {
    if (!_editButton) {
        _editButton  = [UIButton buttonWithType:UIButtonTypeCustom];
        [_editButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_editButton setTitle:@"Edit filters" forState:UIControlStateNormal];
        _editButton.titleLabel.font = [UIFont appFontOfSize:16];
        _editButton.backgroundColor = APP.appMainColor;
        [self.borderView addSubview:_editButton];
    }
    return _editButton;
}

- (void)commonInit {
    self.backgroundColor = APP.backgroundColor;
    self.borderView = [[UIView alloc] init];
    self.borderView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.borderView];
    
    self.textlabel = [[UILabel alloc] init];
    self.textlabel.textColor = APP.defaultTextColor;
    self.textlabel.textAlignment = NSTextAlignmentCenter;
    self.textlabel.font = [UIFont appFontOfSize:15];
    self.textlabel.numberOfLines = 0;
    self.textlabel.text = @"Change your filter to refine your results";
    [self.borderView addSubview:self.textlabel];
    
    [self.borderView.layer addSublayer:self.shapeLayer];
}


- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat selfWidth = self.frame.size.width;
    CGFloat selfHeight = self.frame.size.height;
    
    CGFloat borderWith = [UIScreen mainScreen].bounds.size.width > 320 ? 300 : 270;
    CGFloat borderHeight = 90;
    
    CGFloat margin = 15;
    CGFloat titleY = 20;
    
    if (self.titleLabel) {
        CGSize textlabelSize = [self.titleLabel sizeThatFits:CGSizeMake(borderWith-30, MAXFLOAT)];
        self.titleLabel.frame = CGRectMake(margin, titleY,
                                           borderWith - margin * 2,
                                           textlabelSize.height);
        borderHeight = borderHeight + textlabelSize.height;
    }
    
    CGSize textSize = [self.textlabel sizeThatFits:CGSizeMake(borderWith-30,MAXFLOAT)];
    self.textlabel.frame = CGRectMake(margin,
                                      _titleLabel ? CGRectGetMaxY(self.titleLabel.frame) + 10 : titleY + 5,
                                      borderWith - margin * 2,
                                      textSize.height);
    borderHeight = borderHeight + textSize.height;
    
    if (self.editButton && !self.editButton.hidden) {
        CGFloat editWith = 175;
        self.editButton.frame = CGRectMake((borderWith - editWith) / 2,
                                                 CGRectGetMaxY(self.textlabel.frame) + 25,
                                                 editWith, 44);
        self.editButton.layer.cornerRadius = 4;
        self.editButton.layer.masksToBounds = true;
        borderHeight = borderHeight + 44;
    }
    
    if (!self.editButton || self.editButton.hidden) {
        borderHeight = borderHeight - 30;
    }
    
    self.borderView.frame = CGRectMake((selfWidth - borderWith) / 2,
                                       (selfHeight - borderHeight) / 2,
                                       borderWith, borderHeight);
    self.shapeLayer.path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, borderWith, borderHeight)].CGPath;
    self.shapeLayer.frame = self.bounds;
    
}

- (CAShapeLayer *)shapeLayer {
    if (!_shapeLayer) {
        _shapeLayer = [CAShapeLayer layer];
        _shapeLayer.lineWidth = 0.5f;
        _shapeLayer.lineDashPattern = @[@2,@4];
        _shapeLayer.fillColor = [UIColor clearColor].CGColor;
        _shapeLayer.strokeColor = [UIColor lightGrayColor].CGColor;
    }
    return _shapeLayer;
}

@end
