//
//  AFDBadConnectView.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDBadConnectView.h"

@interface AFDBadConnectView ()

@property (nonatomic, strong) UIImageView *badImageview;
@property (nonatomic, strong) UILabel *descLabel;

@end

@implementation AFDBadConnectView

- (instancetype)init {
    if (self = [super init]) {
        self.backgroundColor = [UIColor colorWithHexRGB:@"#f7f7f7"];
        _descLabel = [[UILabel alloc] init];
        _descLabel.font = [UIFont appLightFontOfSize:15];
        _descLabel.textColor = [APP lightTextColor];
        _descLabel.text = @"Oops! Loading failed.";
        _descLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_descLabel];
        
        _badImageview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LoadingFailed_networking"]];
        [self addSubview:_badImageview];
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)]];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleAFNetworkingReachabilityDidChangeNotification:)
                                                     name:AFNetworkingReachabilityDidChangeNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    self.frame = self.superview.bounds;
}

- (void)tapAction {
    self.hidden = YES;
    if (self.refreshHandler) {
        self.refreshHandler();
    }
}

- (void)handleAFNetworkingReachabilityDidChangeNotification:(NSNotification *)notification {
    if (self.superview && !self.hidden && [AFNetworkReachabilityManager sharedManager].reachable) {
        [self tapAction];
    }
}

- (void)layoutSubviews {
    [_badImageview sizeToFit];
    _badImageview.center = CGPointMake(self.width / 2.f, self.height / 2.f - _badImageview.height / 2.f);
    _descLabel.frame = CGRectMake(20, _badImageview.maxY + 10, self.width - 40, 20);
}

@end
