//
//  AFDBadConnectView.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFDBadConnectView : UIView

@property (nonatomic, copy) void(^refreshHandler)(void);

@end
