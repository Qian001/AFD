//
//  AFDPurchasesManager.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFDPurchasesManager : NSObject

+ (AFDPurchasesManager *)sharedInstance;

@end
