//
//  AFDPurchasesManager.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDPurchasesManager.h"
#import <StoreKit/StoreKit.h>

@interface AFDPurchasesManager ()<SKPaymentTransactionObserver, SKProductsRequestDelegate, SKRequestDelegate>

@end

@implementation AFDPurchasesManager

+ (AFDPurchasesManager *)sharedInstance {
    static AFDPurchasesManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AFDPurchasesManager alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self requestProductsWithCompletion];
    }
    return self;
}


- (void)requestProductsWithCompletion{
//    if (self.productMap.count > 0) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:MSNotificationReceivedProduct object:nil];
//        return;
//    }
//
//    MSReqIAPProduct *request = [[MSReqIAPProduct alloc] init];
//    [request request:^(NSURLSessionDataTask *task, id responseObject) {
//        self.productIdentifiers = responseObject;
//        SKProductsRequest *request= [[SKProductsRequest alloc] initWithProductIdentifiers:
//                                     [NSSet setWithArray:[self.productIdentifiers allKeys]]];
//        request.delegate = self;
//        [request start];
//        self.requestProductdidFail = NO;
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        NSLog(@"%s\n Request IAP Produce failed:%@", __FUNCTION__, error);
//        self.requestProductdidFail = YES;
//        [[NSNotificationCenter defaultCenter] postNotificationName:MSNotificationFailedRequestProduct
//                                                            object:error];
//    }];
}

@end
