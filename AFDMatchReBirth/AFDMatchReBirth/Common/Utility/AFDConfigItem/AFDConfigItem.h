//
//  AFDConfigItem.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AFDConfigItem : NSObject

+ (AFDConfigItem *)shareInstance;

// Color
@property (nonatomic, strong) UIColor *appMainColor;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) UIColor *loadingBackgroundColor;
@property (nonatomic, strong) UIColor *loadingIconColor;
@property (nonatomic, strong) UIColor *defaultTextColor;
@property (nonatomic, strong) UIColor *lightTextColor;
@property (nonatomic, strong) UIColor *tableCellSeparationColor;


// Font


// Setting
@property (nonatomic, strong) NSString *randomAPIKey;


@end
