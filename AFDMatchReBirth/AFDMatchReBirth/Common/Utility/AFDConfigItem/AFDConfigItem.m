//
//  AFDConfigItem.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDConfigItem.h"

@implementation AFDConfigItem

+(AFDConfigItem *)shareInstance {
    static dispatch_once_t onceToken;
    static AFDConfigItem *_instance;
    dispatch_once(&onceToken, ^{
        _instance = [[AFDConfigItem alloc] init];
    });
    return _instance;
}

- (instancetype)init {
    if (self = [super init]) {
        [self setupColors];
        [self setupFonts];
        [self setupOtherOptions];
    }
    return self;
}

- (void)setupColors {
    self.appMainColor = [UIColor colorWithHexRGB:@"#3bb3ec"];
    self.backgroundColor = [UIColor colorWithHexRGB:@"#f7f7f7"];
    self.loadingBackgroundColor = self.backgroundColor;
    self.loadingIconColor = self.appMainColor;
    self.defaultTextColor = [UIColor colorWithHexRGB:@"#1e1e1e"];
    self.lightTextColor =  [UIColor colorWithHexRGB:@"#aaaaaa"];
    self.tableCellSeparationColor = [UIColor colorWithHexRGB:@"#ececec"];
}

- (void)setupFonts {
    
}

- (void)setupOtherOptions {
    self.randomAPIKey = @"";
}

@end
