//
//  AFDLoadingView.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDLoadingView.h"

@interface AFDLoadingView ()

@property (nonatomic, strong)AFDLoadingIcon *loadingIcon;

@end

@implementation AFDLoadingView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.backgroundColor = APP.loadingBackgroundColor;
        self.loadingIcon = [[AFDLoadingIcon alloc] init];
        [self addSubview:_loadingIcon];
    }
    return self;
}

- (UIView *)backgroundview {
    if (!_backgroundview) {
        _backgroundview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
        _backgroundview.backgroundColor = APP.loadingBackgroundColor;
        _backgroundview.layer.cornerRadius = 6;
        _backgroundview.hidden = YES;
        [self insertSubview:_backgroundview belowSubview:_loadingIcon];
    }
    return _backgroundview;
}


- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    [self placeAtVisibleCenter];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self placeAtVisibleCenter];
}

- (void)setTranslucentBackground:(BOOL)translucentBackground {
    if (_translucentBackground == translucentBackground) {
        return;
    }
    _translucentBackground = translucentBackground;
    if (_translucentBackground) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
        self.backgroundview.hidden = NO;
    } else {
        self.backgroundColor = APP.loadingBackgroundColor;
        _backgroundview.hidden = YES;
    }
}

- (void)placeAtVisibleCenter {
    if ([self.superview isKindOfClass:[UIScrollView class]]) {
        UIScrollView *scrollView = (UIScrollView *)self.superview;
        self.frame = (CGRect){CGPointZero, self.superview.bounds.size.width, MAX(scrollView.height, scrollView.contentSize.height)};
    } else {
        self.frame = (CGRect){CGPointZero, self.superview.bounds.size};
    }
    self.loadingIcon.center = self.superview.visibleCenter;
    _backgroundview.center = self.superview.visibleCenter;
}

- (void)beginAnamtion {
    self.hidden = NO;
    [self.superview bringSubviewToFront:self];
    [self placeAtVisibleCenter];
    [self.loadingIcon beginAnimation];
}

- (void)stopAnimation {
    self.hidden = YES;
    [self.superview sendSubviewToBack:self];
    [self.loadingIcon stopAnimation];
}

@end

@interface AFDLoadingIcon ()

@property (nonatomic, strong) CALayer *coloredLayer;
@property (nonatomic, strong) CAGradientLayer *glayerA;
@property (nonatomic, strong) CAGradientLayer *glayerB;
@property (nonatomic, strong) CAShapeLayer * tflayer;
@property (nonatomic, strong) CABasicAnimation *animation;

@end

@implementation AFDLoadingIcon

- (instancetype)init {
    self = [super initWithFrame:CGRectMake(0, 0, 35, 35)];
    if (self) {
        _glayerA = [CAGradientLayer layer];
        
        _glayerA.colors = @[(id)[[APP loadingIconColor] colorWithAlphaComponent:0.1].CGColor,
                            (id)[[APP loadingIconColor] colorWithAlphaComponent:0.4].CGColor];
        _glayerA.startPoint = CGPointMake(0.5, 0);
        _glayerA.endPoint = CGPointMake(0.5, 1);
        _glayerA.locations = @[@(0.2), @(0.8)];
        
        _glayerB = [CAGradientLayer layer];
        _glayerB.colors = @[(id)[[APP loadingIconColor] colorWithAlphaComponent:0.7].CGColor,
                            (id)[[APP loadingIconColor] colorWithAlphaComponent:0.4].CGColor];
        _glayerB.startPoint = CGPointMake(0.5, 0);
        _glayerB.endPoint = CGPointMake(0.5, 1);
        _glayerB.locations = @[@(0.2), @(0.8)];
        
        _coloredLayer = [CALayer layer];
        [_coloredLayer addSublayer:_glayerA];
        [_coloredLayer addSublayer:_glayerB];
        
        _tflayer = [CAShapeLayer layer];
        _tflayer.lineWidth = 5;
        _tflayer.strokeStart = 0.02;
        _tflayer.strokeEnd = 0.98;
        _tflayer.lineCap = kCALineCapRound;
        _tflayer.fillColor = [UIColor clearColor].CGColor;
        _tflayer.strokeColor = [UIColor whiteColor].CGColor;
        
        _coloredLayer.mask = _tflayer;
        
        _glayerA.contentsScale =
        _glayerB.contentsScale =
        _coloredLayer.contentsScale =
        _tflayer.contentsScale = [UIScreen mainScreen].scale;
        
        _animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        _animation.fromValue = @(0);
        _animation.toValue = @(M_PI * 2);
        _animation.duration = 1;
        _animation.repeatCount = HUGE_VAL;
        _animation.removedOnCompletion = NO;
        [self.layer addSublayer:_coloredLayer];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _coloredLayer.frame = self.bounds;
    _tflayer.frame = self.bounds;
    _glayerA.frame = CGRectMake(self.width / 2.f, 0, self.width / 2.f, self.height);
    _glayerB.frame = CGRectMake(0, 0, self.width / 2.f, self.height);
    
    _tflayer.path = [UIBezierPath bezierPathWithArcCenter:self.boundsCenter
                                                   radius:self.width / 2 - _tflayer.lineWidth / 2.f
                                               startAngle:-M_PI / 4.f
                                                 endAngle:M_PI * 1.5
                                                clockwise:YES].CGPath;
}

- (void)beginAnimation {
    [self.layer removeAllAnimations];
    [self.layer addAnimation:_animation forKey:nil];
}

- (void)stopAnimation {
    [self.layer removeAllAnimations];
}

@end
