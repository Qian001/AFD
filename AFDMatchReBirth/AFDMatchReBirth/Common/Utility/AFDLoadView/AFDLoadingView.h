//
//  AFDLoadingView.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFDLoadingView : UIView

@property (nonatomic, strong) UIView* backgroundview;
@property (nonatomic, assign) BOOL translucentBackground;
- (void)beginAnamtion;
- (void)stopAnimation;

@end

@interface AFDLoadingIcon : UIView

- (void)beginAnimation;
- (void)stopAnimation;

@end
