//
//  AFDGetRequest.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDGetRequest.h"

@implementation AFDGetRequest

- (NSURLSessionDataTask *)request:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    self.beginDate = [NSDate date];
    NSURLSessionDataTask *task = [self.HTTPSessionManager GET:self.path parameters:self.params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self logTimeForTask:task whenReceivedResponse:responseObject];
        id object = [self processResponse:responseObject];
        if (success) {
            success(task, object);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self logTimeForTask:task whenEncounterError:error];
        [self task:task error:error success:success failure:failure];
    }];
    return task;
}

@end
