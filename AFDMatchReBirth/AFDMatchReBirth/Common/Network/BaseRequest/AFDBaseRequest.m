//
//  AFDBaseRequest.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDBaseRequest.h"
#import "AFDJSONResponseSerializer.h"

@interface AFDBaseRequest ()

@property (nonatomic, strong) NSDate *endDate;

@end

@implementation AFDBaseRequest {
    NSMutableDictionary *_params;
}

- (id)init {
    self = [super init];
    if (self) {
        self.sessionRequired = YES;
        AFDSessionStatus sessionStatus = [AFDSessionManager shareManager].sessionStatus;
        if (sessionStatus != AFDSessionStatusValid) {
            [[AFDSessionManager shareManager].refreshSessionRequests addObject:self];
            NSLog(@"Number of refreshSessionRequests %lu. Adding %@", (unsigned long)[AFDSessionManager shareManager].refreshSessionRequests.count, [self class]);
        }
    }
    return self;
}

- (NSURLSessionDataTask *)request:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    return  nil;
}

- (AFHTTPSessionManager *)HTTPSessionManager {
    NSURL *previousURL = _HTTPSessionManager.baseURL;
    NSString *previous = [previousURL.absoluteString hasSuffix:@"/"] ? previousURL.absoluteString : [previousURL.absoluteString stringByAppendingString:@"/"];
    NSURL *currentURL = [AFDSessionManager shareManager].baseURL;
    NSString *current = [currentURL.absoluteString hasSuffix:@"/"] ? currentURL.absoluteString : [currentURL.absoluteString stringByAppendingString:@"/"];
    if (_HTTPSessionManager == nil || ![previous isEqualToString:current]) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        config.HTTPShouldSetCookies = NO;
        config.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicyNever;
        NSURL *baseURL = [AFDSessionManager shareManager].baseURL;
        _HTTPSessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL sessionConfiguration:config];
        
        [_HTTPSessionManager setResponseSerializer:[AFDJSONResponseSerializer serializer]];
        _HTTPSessionManager.requestSerializer.timeoutInterval = AFDRequestTimeoutInterval;
        NSString *host = baseURL.host;
        NSString *path = [baseURL.path stringByAppendingPathComponent:[NSURL URLWithString:self.path].path];
        NSString *checksum = [self checkSumCodeForHost:host path:path randomCode:APP.randomAPIKey url:@"XX"];
        [_HTTPSessionManager.requestSerializer setValue:checksum forHTTPHeaderField:@"randomstr"];
        NSString *buildVersion = @([[AFDAppInfoManager buildVersion] integerValue]).description;
        NSString *branchNubmer = [[AFDAppInfoManager appBranchName] isPureInt] ? [AFDAppInfoManager appBranchName] : @"";
        [_HTTPSessionManager.requestSerializer setValue:branchNubmer forHTTPHeaderField:@"r-version"];
        NSString *userAgent = [NSString stringWithFormat:@"%@/%@ (%@; iOS %@; %@; %@; %@)", [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleExecutableKey] ?: [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleIdentifierKey], [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] ?: [[NSBundle mainBundle] infoDictionary][(__bridge NSString *)kCFBundleVersionKey], [UIDevice deviceType], [[UIDevice currentDevice] systemVersion], buildVersion, [AFDAppInfoManager appPlatform], branchNubmer];
        [_HTTPSessionManager.requestSerializer setValue:userAgent forHTTPHeaderField:@"User-Agent"];
        [_HTTPSessionManager.requestSerializer setValue:[AFDAppInfoManager appPlatform] forHTTPHeaderField:@"platform"];
        [_HTTPSessionManager.requestSerializer setValue:buildVersion forHTTPHeaderField:@"app-build"];
        if (@available(iOS 10, *)) {
            [_HTTPSessionManager.requestSerializer setValue:[NSLocale autoupdatingCurrentLocale].languageCode forHTTPHeaderField:@"language-code"];
        }
    }
    return _HTTPSessionManager;
}

- (void)dealloc {
    [_params removeAllObjects];
    _params = nil;
}

- (NSMutableDictionary *)params {
    _params = [NSMutableDictionary dictionary];
    return _params;
}

- (id)processResponse:(id)object {
    return object;
}

- (void)logTimeForTask:(NSURLSessionDataTask *) task whenReceivedResponse:(id) responseObject {
    self.endDate = [NSDate date];
    if ([[AFDSessionManager shareManager].refreshSessionRequests containsObject:self]) {
        if ([AFDSessionManager shareManager].session.length > 0 && self.isSessionRequired) {
            [AFDSessionManager shareManager].sessionStatus = AFDSessionStatusValid;
        } else {
            [AFDSessionManager shareManager].sessionStatus = AFDSessionStatusUnknown;
        }
        
        [[AFDSessionManager shareManager].refreshSessionRequests removeObject:self];
        NSLog(@"Number of refreshSessionRequests %lu. Removing %@", (unsigned long)[AFDSessionManager shareManager].refreshSessionRequests.count, [self class]);
    }
//    if ([self isMemberOfClass:[MSReqLogout class]]) {
//        [MSSessionManager shareManager].sessionStatus = MSSessionStatusInvalid;
//    }
}

- (void)logTimeForTask:(NSURLSessionDataTask *)task whenEncounterError:(NSError *)error {
    self.endDate = [NSDate date];
    NSMutableString *log = [[NSMutableString alloc] initWithFormat:@" %@ URL: %@ \n begin date: %@ \n end date: %@ \n time costed: %f \n allHTTPHeaderFields: %@ \n params: %@ \n error: %@", [self class], task.currentRequest.URL, [self.beginDate stringWithFormat:@"yyyy-MM-dd HH:mm:ss"] , [self.endDate stringWithFormat:@"yyyy-MM-dd HH:mm:ss"], [self.endDate timeIntervalSinceDate:self.beginDate], task.currentRequest.allHTTPHeaderFields, self.params, error];
    NSLog(@"%@", log);
}

- (void)task:(NSURLSessionDataTask *)task error:(NSError *)error success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSMutableArray *refreshSessionRequests = [AFDSessionManager shareManager].refreshSessionRequests;
    AFDSessionStatus sessionStatus = [AFDSessionManager shareManager].sessionStatus;
    if ([error.domain isEqualToString:AFDRequestionErrorDomain]) {
        if (error.code == ErrorCode_100) {
            if ([refreshSessionRequests containsObject:self] && sessionStatus != AFDSessionStatusValid && self.isSessionRequired) {
                [AFDSessionManager shareManager].sessionStatus = AFDSessionStatusExpired;
            }
            
            [[AFDSessionManager shareManager] reLoginSuccess:^(NSURLSessionDataTask *task, id responseObject) {
                [self request:^(NSURLSessionDataTask *task, id responseObject) {
                    if (success) {
                        success(task, responseObject);
                    }
                } failure:^(NSURLSessionDataTask *task, NSError *error) {
                    if (failure) {
                        failure(task, error);
                    }
                }];
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                if(failure) failure(task, error);
            }];
        } else if (error.code == ErrorCode_Positive_1 ||error.code == ErrorCode_Positive_2 || error.code == ErrorCode_Positive_4 || error.code == ErrorCode_Positive_6 || error.code == ErrorCode_Positive_7) {
            if (self.isSessionRequired) [AFDSessionManager shareManager].sessionStatus = AFDSessionStatusInvalid;
            [AFDSessionManager shareManager].logoutManually = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:TFNotificationUserLogOutSuccess object:nil userInfo:nil];
            if (failure) {
                failure(task, error);
            }
        } else if (error.code == ErrorCode_101 || error.code == ErrorCode_102 || error.code == ErrorCode_103 || error.code == ErrorCode_107 || error.code == ErrorCode_125 || error.code == ErrorCode_127 || error.code == ErrorCode_Positive_18) {
            if ([refreshSessionRequests containsObject:self]) {
                if (self.isSessionRequired) [AFDSessionManager shareManager].sessionStatus = AFDSessionStatusInvalid;
                [AFDSessionManager shareManager].logoutManually = YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:TFNotificationUserLogOutSuccess object:nil];
            }
            if (failure) {
                failure(task, error);
            }
        } else {
            if (self.isSessionRequired) [AFDSessionManager shareManager].sessionStatus = AFDSessionStatusValid;
            if (failure) {
                failure(task, error);
            }
        }
    } else {
        if ([refreshSessionRequests containsObject:self] && sessionStatus != AFDSessionStatusValid && self.isSessionRequired) {
            [AFDSessionManager shareManager].sessionStatus = AFDSessionStatusRefreshFailed;
        }
        
        if (failure) {
            failure(task, error);
        }
    }
    if ([refreshSessionRequests containsObject:self]) {
        [refreshSessionRequests removeObject:self];
        NSLog(@"Number of refreshSessionRequests %lu. Removing %@", (unsigned long)[AFDSessionManager shareManager].refreshSessionRequests.count, [self class]);
    }
}

- (NSString *)checkSumCodeForHost:(NSString *)host path:(NSString *)path randomCode:(NSString *)randomCode url:(NSString *)url {

    NSString *base64;
    NSString *timestamp = [NSString stringWithFormat:@"%.0f", [[NSDate date] timeIntervalSince1970]];
    
    NSString *all = [NSString stringWithFormat:@"%@%@%@%@%@",
                     path.lowercaseString,
                     host.lowercaseString,
                     randomCode.lowercaseString,
                     timestamp,
                     url.lowercaseString];
    
    NSString *sha1 = [all sha1];
    NSString *md5 = [[NSString stringWithFormat:@"%@%@%@", sha1, [timestamp md5], url.lowercaseString] md5];
    base64 = [[[NSString stringWithFormat:@"%@%@", md5, timestamp] dataUsingEncoding:NSUTF8StringEncoding] base64EncodedStringWithOptions:0];
    NSLog(@"host: %@ \npath: %@\nall: %@ \nsha1: %@ \ntime: %@\nmd5_time_sting: %@\nmd5: %@ \nbase64: %@", host, path, all, sha1, timestamp, [timestamp md5], md5, base64);
    return base64;
}

@end
