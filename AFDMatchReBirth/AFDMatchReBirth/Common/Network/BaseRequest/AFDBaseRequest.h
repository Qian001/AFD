//
//  AFDBaseRequest.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import <UIKit/UIKit.h>

#if DEBUG
// TODO: DEBUG
#define AFDRequestTimeoutInterval 30
#else
#define AFDRequestTimeoutInterval 60
#endif

@interface AFDBaseRequest : NSObject

@property (nonatomic, strong) NSDate *beginDate;
@property (nonatomic, assign, getter=isSessionRequired) BOOL sessionRequired;
@property (nonatomic, copy) NSString *path;
@property (nonatomic, strong, readonly) NSMutableDictionary *params;
@property (nonatomic, strong) AFHTTPSessionManager *HTTPSessionManager;

- (id)processResponse:(id)object;

- (void)task:(NSURLSessionDataTask *)task error:(NSError *)error success:(void (^)(NSURLSessionDataTask *task, id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (NSURLSessionDataTask *)request:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                          failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

- (void)logTimeForTask:(NSURLSessionDataTask *)task whenReceivedResponse:(id) responseObject;

- (void)logTimeForTask:(NSURLSessionDataTask *)task whenEncounterError:(NSError *)error;

@end
