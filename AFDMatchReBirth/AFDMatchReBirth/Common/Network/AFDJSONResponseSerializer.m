//
//  AFDJSONResponseSerializer.m
//  TFMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDJSONResponseSerializer.h"

@implementation AFDJSONResponseSerializer

- (id)responseObjectForResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *__autoreleasing *)error {
    self.readingOptions = NSJSONReadingMutableContainers;
    self.removesKeysWithNullValues = YES;
    NSMutableDictionary *json = [super responseObjectForResponse:response data:data error:error];
    
    if ([json isKindOfClass:[NSDictionary class]] == NO) {
        *error = [NSError errorWithDomain:AFDRequestionErrorDomain
                                     code:ErrorCode_11
                                  message:NSLocalizedString(@"request_failed", nil)];
        NSMutableString *log = [[NSMutableString alloc] initWithFormat:@"URL: %@ \n primitive json response \n%@", response.URL, [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding]];
        NSLog(@"%s\n%@", __FUNCTION__, log);
        return nil;
    }
    if ([json objectForKey:@"errs"]) {
        NSMutableArray *errs = [[NSMutableArray alloc] init];
        if (![[json objectForKey:@"errs"] isKindOfClass:[NSMutableArray class]]) {
            return  nil;
        }
        for (NSDictionary *jsonErr in [json objectForKey:@"errs"]) {
            NSError *err = [NSError errorWithDomain:AFDRequestionErrorDomain
                                               code:[[jsonErr objectForKey:@"errcode"] intValue]
                                            message:[jsonErr objectForKey:@"errmsg"]];
            [errs addObject:err];
            [self handleResponsError:err];
        };
        *error = [NSError errorWithErrors:errs];
        return nil;
    } else if ([json objectForKey:@"errcode"] || [json objectForKey:@"errmsg"]) {
        *error = [NSError errorWithDomain:AFDRequestionErrorDomain
                                     code:[[json objectForKey:@"errcode"] intValue]
                                  message:[json objectForKey:@"errmsg"]];
        [self handleResponsError:*error];
        return nil;
    } else if ([json objectForKey:@"res"]) {
        NSDictionary *errDictionary;
        if ([[json objectForKey:@"res"] isKindOfClass:[NSString class]]) {
            NSString *errorDescription = (NSString *)[json objectForKey:@"res"];
            errDictionary = [NSJSONSerialization JSONObjectWithData:[errorDescription dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
            if (errDictionary) {
                *error = [NSError errorWithDomain:AFDRequestionErrorDomain
                                             code:[[errDictionary objectForKey:@"errcode"] intValue]
                                          message:[errDictionary objectForKey:@"errmsg"]];
                [self handleResponsError:*error];
                return nil;
            }
        }
    }
    return [[self class] replaceNull:json];
}

- (void)handleResponsError:(NSError *)error {
    if (error.code == ErrorCode_119) {
        dispatch_async(dispatch_get_main_queue(), ^{
//            [MSProfileManager sharedProfileManager].profile.status.gold = NO;
//            [[MSProfileManager sharedProfileManager] storeProfile];
//            [[NSNotificationCenter defaultCenter] postNotificationName:MSNotificationMembershipChanged object:nil];
        });
    }
}

+ (id)replaceNull:(id)dict {
    if ([dict isKindOfClass:[NSDictionary class]]){
        NSMutableDictionary *tmpDic = [[NSMutableDictionary alloc] init];
        for (NSString *key in [dict allKeys]){
            if (![dict[key] isKindOfClass:[NSNull class]]){
                id dicValue = [self replaceNull: dict[key]];
                if (dicValue != nil) {
                    [tmpDic setObject: dicValue forKey:key];
                }
            }
        }
        return tmpDic.count > 0 ? tmpDic : nil;
    } else if ([dict isKindOfClass:[NSArray class]]) {
        NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
        for (id array in dict) {
            id arrayValue = [self replaceNull: array];
            if (arrayValue != nil) {
                [tmpArray addObject:arrayValue];
            }
        }
        return tmpArray.count > 0 ? tmpArray : @[];
    }
    return dict;
}

@end
