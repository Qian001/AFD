//
//  AFDJSONResponseSerializer.h
//  TFMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFNetworking.h"

@interface AFDJSONResponseSerializer : AFJSONResponseSerializer

+ (id)replaceNull:(id)dict;

@end
