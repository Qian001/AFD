//
//  AFDSessionManager.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, AFDSessionStatus) {
    AFDSessionStatusUnknown,
    AFDSessionStatusValid,
    AFDSessionStatusInvalid,
    AFDSessionStatusExpired,
    AFDSessionStatusRefreshFailed,
};

@interface AFDSessionManager : NSObject

@property (nonatomic, copy) NSString *session;
@property (nonatomic, strong, readonly) NSURL *baseURL;
@property (nonatomic, assign, getter=isLogoutManually) BOOL logoutManually;
@property (nonatomic, assign, getter=isSavingPrivacy) BOOL savingPrivacy;
@property (nonatomic, strong) void (^privacySavedHandler) (BOOL succeed);

@property (nonatomic, assign) AFDSessionStatus sessionStatus;
@property (nonatomic, strong) NSMutableArray *refreshSessionRequests;

- (void)reLoginSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success
               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

+ (AFDSessionManager *)shareManager;

+ (void)clearUp;

- (void)showElsewhereLoginAlertView:(NSString *)newUsername dismiss:(void(^)(void))dismiss;

@end
