//
//  AFDSessionManager.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDSessionManager.h"
#import "AFDPresentAlertView.h"

@interface AFDSessionManager()

@property (nonatomic, strong) NSString *baseUrlString;
@property (nonatomic, assign) BOOL havePressentAlertView;

@end

@implementation AFDSessionManager

+ (AFDSessionManager *)shareManager {
    static dispatch_once_t onceToken;
    static AFDSessionManager *instance;
    dispatch_once(&onceToken, ^{
        instance = [[AFDSessionManager alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        _session = [AFDUserDefaults userDefaultsBy:AFDPersistenceSessionData];
        NSString *urlString = [AFDAppInfoManager host];
        NSAssert(urlString, @"Please set Base URL at TFHostKeyInPlist, key is TFHostList");
        _baseUrlString = urlString;
        _sessionStatus = AFDSessionStatusUnknown;
        _refreshSessionRequests = [@[]mutableCopy];
        _havePressentAlertView = NO;
    }
    return self;
}

- (void)setSession:(NSString *)session {
    _session = [session copy];
    [AFDUserDefaults saveUserDefaults:AFDPersistenceSessionData value:session];
    [[AFDRemoteNotificationManager sharedManager] updateToken];
}

- (void)setSessionStatus:(AFDSessionStatus)sessionStatus {
    NSString *description;
    switch (sessionStatus) {
        case AFDSessionStatusUnknown:
            description = @"MSSessionStatusUnknown";
            break;
        case AFDSessionStatusValid:
            description = @"MSSessionStatusValid";
            break;
        case AFDSessionStatusExpired:
            description = @"MSSessionStatusExpired";
            break;
        case AFDSessionStatusInvalid:
            description = @"MSSessionStatusInvalid";
            break;
        case AFDSessionStatusRefreshFailed:
            description = @"MSSessionStatusRefreshFailed";
            break;
        default:
            description = @"";
            break;
    }
    NSLog(@"current session is %@, sessionStatus is %ld description: %@", self.session, (unsigned long)sessionStatus, description);
    [[NSNotificationCenter defaultCenter] postNotificationName:TFSessionStatusWillChangeNotification object:self userInfo:nil];
    _sessionStatus = sessionStatus;
    [[NSNotificationCenter defaultCenter] postNotificationName:TFSessionStatusDidChangedNotification object:self userInfo:nil];
}

- (NSURL *)baseURL {
    NSString *urlString = _baseUrlString;
    NSURL *baseUrL = [NSURL URLWithString:urlString];
    if (self.session) {
        baseUrL = [baseUrL URLByAppendingPathComponent:self.session];
    }
    NSString *apiVersion = [AFDAppInfoManager apiVersion];
    if (apiVersion) {
        baseUrL = [baseUrL URLByAppendingPathComponent:apiVersion];
    }
    return baseUrL;
}

- (void)reLoginSuccess:(void (^)(NSURLSessionDataTask *task, id responseObject))success
               failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
//    dispatch_async(dispatch_get_main_queue(), ^{
//        MSProfileAccountModel *account = [MSProfileManager sharedProfileManager].profile.account;
//        if (account.username && account.password) {
//            MSReqLogin *requestLogin = [[MSReqLogin alloc] init];
//            requestLogin.username = account.username;
//            requestLogin.password = account.password;
//            requestLogin.rememberPassword = [MSProfileManager passwordOfLastUser] != nil;
//            [requestLogin request:^(NSURLSessionDataTask *task, id responseObject) {
//                if (success) {
//                    success(task, responseObject);
//                }
//            } failure:^(NSURLSessionDataTask *task, NSError *error) {
//                if (error.code == ErrorCode_128 && !_havePressentAlertView) {
//                    __weak typeof(self) weakSelf = self;
//                    NSRange range = [error.message rangeOfString:@"You have changed username to: "];
//                    NSString *newUsername;
//                    if (range.length > 0 && error.message.length > range.length) {
//                        newUsername = [error.message substringFromIndex:range.length];
//                    }
//
//                    if (newUsername) {
//                        [MSProfileManager sharedProfileManager].profile.account.username = newUsername;
//                        [[MSProfileManager sharedProfileManager] storeProfile];
//                    }
//
//                    [weakSelf showChangedUsernameAlertViewWithNewUsername:newUsername buttonDismissCompletion:^(id alertView, NSInteger buttonIndex) {
//                        [weakSelf reLoginSuccess:^(NSURLSessionDataTask *task, id responseObject) {
//                            [[MSXMPPServiceCoreDataManager shareManager].service disconnect];
//                            [[MSXMPPServiceCoreDataManager shareManager].service connect];
//                            if (success) {
//                                success(task, responseObject);
//                            }
//                        } failure:^(NSURLSessionDataTask *task, NSError *error) {
//                            if (failure) {
//                                failure(task, error);
//                            }
//                        }];
//                        [[NSNotificationCenter defaultCenter] postNotificationName:MSNotificationUserChangedUserName object:nil];
//                    }];
//                } else {
//                    if (failure) {
//                        failure(task, error);
//                    }
//                }
//            }];
//        } else {
//            [MSSessionManager shareManager].logoutManually = YES;
//            [[NSNotificationCenter defaultCenter] postNotificationName:MSNotificationUserLogOutSuccess object:nil];
//        }
//    });
}

- (void)showElsewhereLoginAlertView:(NSString *)newUsername dismiss:(void(^)(void))dismiss {
    if (_havePressentAlertView) {
        return;
    }
    _havePressentAlertView = YES;
    
    AFDPresentAlertView *alerView = [[AFDPresentAlertView alloc]initWithButtonItems:@[[AFDAlertButtonItem buttonItemWithTitle:NSLocalizedString(@"OK", nil) action:^{
        
    } bold:false]]];

    alerView.titleLabel.text = NSLocalizedString(@"Congratulations!", nil);
    
    NSString *content = [NSString stringWithFormat:NSLocalizedString(@"Your_Username_Changed_Successfully", nil), newUsername];
    
    NSMutableAttributedString *aString = [[NSMutableAttributedString alloc] initWithString:content];
    [aString addAttribute:NSForegroundColorAttributeName value:APP.appMainColor range:[content rangeOfString:newUsername]];
    alerView.textLabel.attributedText = aString;

    alerView.isTapToDismiss = NO;
    
    [alerView dismissAnimated:true completion:^{
        self.havePressentAlertView = NO;
        if (dismiss) {
            dismiss();
        }
    }];
    [alerView presentAnimated:YES completion:nil];
}

+ (void)clearUp {
    [AFDSessionManager shareManager].session = nil;
}

@end
