//
//  AFDRemoteNotificationModel.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDRemoteNotificationModel.h"

@implementation AFDRemoteNotificationModel

- (instancetype)initWithRemoteInfo:(NSDictionary *)info {
    self = [super init];
    if (self) {
        self.type = [[info objectForKey:@"type"] integerValue];
        self.message = [[info objectForKey:@"aps"] objectForKey:@"alert"];
        self.body = [info objectForKey:@"body"];
        self.sender = [info objectForKey:@"sender"];
        self.senderid = [info objectForKey:@"senderid"];
        self.matchId = [[info objectForKey:@"match_id"] integerValue];
        self.picURL = [NSURL URLWithString:[info objectForKey:@"pic_url"]];
        self.iconURL = [NSURL URLWithString:[info objectForKey:@"icon"]];
//        self.gender = (MSGender)[[info objectForKey:@"gender"] integerValue];
    }
    return self;
}

- (instancetype)initWithLocalNotification:(NSDictionary *)info {
    self = [super init];
    if (self) {
        self.type = [[info objectForKey:@"type"] integerValue];
        self.message = [[info objectForKey:@"aps"] objectForKey:@"alert"];
        self.body = [info objectForKey:@"body"];
        self.sender = [[info objectForKey:@"message"] objectForKey:@"sender"];
        self.senderid = [info objectForKey:@"senderid"];
        self.matchId = [[info objectForKey:@"match_id"] integerValue];
        self.picURL = [NSURL URLWithString:[info objectForKey:@"pic_url"]];
        self.iconURL = [NSURL URLWithString:[info objectForKey:@"icon"]];
//        self.gender = (MSGender)[[info objectForKey:@"gender"] integerValue];
    }
    return self;
}

@end
