//
//  AFDRemoteNotificationModel.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, AFDNotificationType) {
    AFDNotificationTypeMessage = 1,
    AFDNotificationTypeWink = 2,
    AFDNotificationTypeIntrestedInMe = 3,
    AFDNotificationTypeVisitor = 4,
    AFDNotificationTypeRequestYourPrivatePhotos = 5,
    AFDNotificationTypeOpenPrivateAblumToYou = 6,
    AFDNotificationTypeNewMatch = 7,
    AFDNotificationTypeNewActivityLike = 8,
    AFDNotificationTypeNewMomentsComment = 9,
    AFDNotificationTypeNewGift = 13,
    AFDNotificationTypeRemindUnhideProfile = 20,
    AFDNotificationTypeNewRoundIsReady = 1000,
};

@interface AFDRemoteNotificationModel : NSObject

@property (copy, nonatomic) NSString *message;
@property (copy, nonatomic) NSString *body;
@property (copy, nonatomic) NSString *sender;
@property (copy, nonatomic) NSString *senderid;
@property (assign, nonatomic) NSInteger matchId;
@property (strong, nonatomic) NSURL *picURL;
@property (strong, nonatomic) NSURL *iconURL;
//@property (assign, nonatomic) MSGender gender;
@property (assign, nonatomic) AFDNotificationType type;

- (instancetype)initWithRemoteInfo:(NSDictionary *)remoteInfo;

- (instancetype)initWithLocalNotification:(NSDictionary *)info;


@end
