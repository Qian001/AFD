//
//  AFDMyProfileModel.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/3.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDMyProfileModel.h"
#import "AFDMyProfileModel+Wcdb.h"

@implementation UserModel

- (instancetype)init {
    if (self = [super init]) {
        self.name = @"seedyshen";
    }
    return self;
}
WCDB_IMPLEMENTATION(UserModel)

WCDB_SYNTHESIZE(UserModel, name)
// 用于定义主键
WCDB_PRIMARY(UserModel, name)

@end

@interface AFDMyProfileModel ()<WCTTableCoding>

//@property (nonatomic, strong) WCTDatabase *database;

@end

@implementation AFDMyProfileModel

// 使用 WCDB_IMPLEMENTATIO 宏在类文件定义绑定到数据库表的类
WCDB_IMPLEMENTATION(AFDMyProfileModel)
// 使用 WCDB_SYNTHESIZE 宏在类文件定义需要绑定到数据库表的字段
WCDB_SYNTHESIZE(AFDMyProfileModel, messageId)
WCDB_SYNTHESIZE(AFDMyProfileModel, title)
WCDB_SYNTHESIZE(AFDMyProfileModel, createTime)
WCDB_SYNTHESIZE(AFDMyProfileModel, content)
WCDB_SYNTHESIZE(AFDMyProfileModel, count)
WCDB_SYNTHESIZE(AFDMyProfileModel, userID)
WCDB_SYNTHESIZE(AFDMyProfileModel, userModel)
// 用于定义主键
WCDB_PRIMARY(AFDMyProfileModel, userID)


+ (AFDMyProfileModel *)shareInstance {
    static dispatch_once_t onceToken;
    static AFDMyProfileModel *_instance;
    dispatch_once(&onceToken, ^{
        _instance = [[AFDMyProfileModel alloc] init];
    });
    return _instance;
}

- (instancetype)init {
    if (self = [super init]) {
        self.userID = @"001";
    }
    return self;
}

+ (NSString *)wcdbFilePath {
    NSArray *filePath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [filePath objectAtIndex:0];
    NSString *dbFilePath = [documentPath stringByAppendingPathComponent:@"AFDMyProfileModel.db"];
    return dbFilePath;
}

+ (BOOL)creatDatabase {
    WCTDatabase *database = [[WCTDatabase alloc] initWithPath:[self wcdbFilePath]];
    if (![database isTableExists:[self tableWcdbName]]) {
        BOOL result = [database createTableAndIndexesOfName:[self tableWcdbName] withClass:AFDMyProfileModel.class];
        return result;
    }
    return YES;
}


+ (NSString*)tableWcdbName {
    return @"TFMyProfile_tableDatabase";
}

- (BOOL)updateUserProfile {
    WCTDatabase *database = [[WCTDatabase alloc] initWithPath:[[self class] wcdbFilePath]];
    if (![self isHasBeenTab]) {
        [[self class] creatDatabase];
    }
    BOOL result = true;
    if ([self isHasBeenUser]) {
        BOOL tempResult =  [database updateRowsInTable:[[self class] tableWcdbName] onProperties:[AFDMyProfileModel AllProperties] withObject:self where:AFDMyProfileModel.userID == self.userID];
//        [database updateRowsInTable:[[self class] tableWcdbName] onProperties:TFMyProfileModel.content withObject:self where:TFMyProfileModel.userID == self.userID];
        result = result && tempResult;
        return result;
    }else {
        return [self insertUserProfile];
    }
}

- (BOOL)insertUserProfile {
    WCTDatabase *database = [[WCTDatabase alloc] initWithPath:[[self class] wcdbFilePath]];
    if (![self isHasBeenTab]) {
        [[self class] creatDatabase];
    }
    BOOL result = [database insertObject:self into:[[self class] tableWcdbName]];
    return result;
}

- (BOOL)deleteUserProfile {
    WCTDatabase *database = [[WCTDatabase alloc] initWithPath:[[self class] wcdbFilePath]];
    if (!database) {
        [[self class] creatDatabase];
    }
    BOOL result = [database deleteObjectsFromTable:[[self class] tableWcdbName] where:AFDMyProfileModel.userID == self.userID];
    return result;
}

- (NSArray<AFDMyProfileModel *> *)getAllData {
    WCTDatabase *database = [[WCTDatabase alloc] initWithPath:[[self class] wcdbFilePath]];
    if (![self isHasBeenTab]) {
        [[self class] creatDatabase];
    }
    return [database getAllObjectsOfClass:AFDMyProfileModel.class fromTable:[[self class] tableWcdbName]];
}

- (AFDMyProfileModel *)wcdbMyprofile {
    NSArray *users = [self getAllData];
    for (AFDMyProfileModel *userModel in users) {
        if ([userModel.userID isEqualToString:self.userID]) return userModel;
    }
    return [AFDMyProfileModel shareInstance];
}

- (BOOL)isHasBeenUser {
    NSArray *users = [self getAllData];
    BOOL result = NO;
    for (AFDMyProfileModel *userModel in users) {
        if ([userModel.userID isEqualToString:self.userID]) result = true;
    }
    return result;
}

- (BOOL)isHasBeenTab {
    WCTDatabase *database = [[WCTDatabase alloc] initWithPath:[[self class] wcdbFilePath]];
    return [database isTableExists:[[self class] tableWcdbName]];
}

@end
