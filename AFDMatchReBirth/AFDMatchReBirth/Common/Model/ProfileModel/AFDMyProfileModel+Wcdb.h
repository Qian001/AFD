//
//  AFDMyProfileModel+Wcdb.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/3.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDMyProfileModel.h"
#import <WCDB/WCDB.h>

@interface AFDMyProfileModel (Wcdb)

WCDB_PROPERTY(userID)
WCDB_PROPERTY(messageId)
WCDB_PROPERTY(title)
WCDB_PROPERTY(createTime)
WCDB_PROPERTY(content)
WCDB_PROPERTY(count)
WCDB_PROPERTY(userModel)

@end

@interface UserModel (Wcdb)

WCDB_PROPERTY(name)

@end
