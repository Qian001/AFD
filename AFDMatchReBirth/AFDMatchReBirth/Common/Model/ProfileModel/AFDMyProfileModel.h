//
//  AFDMyProfileModel.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/3.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

@property (nonatomic, strong) NSString *name;

//WCDB_PROPERTY(name)

@end

@interface AFDMyProfileModel : NSObject

@property (nonatomic, strong) NSString *userID;

@property (nonatomic, assign) NSInteger messageId;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, assign) NSInteger createTime;

@property (nonatomic, strong) NSString *content;

@property (nonatomic, assign) NSInteger count;

@property (nonatomic, strong) UserModel *userModel;

+ (AFDMyProfileModel *)shareInstance;

- (BOOL)updateUserProfile;

- (AFDMyProfileModel *)wcdbMyprofile;

@end
