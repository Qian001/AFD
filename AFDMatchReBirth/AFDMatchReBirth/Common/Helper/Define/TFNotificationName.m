//
//  TFNotification.m
//  TFMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "TFNotificationName.h"

@implementation TFNotificationName


// TFSessionManager
NSString * const TFSessionStatusWillChangeNotification = @"TFSessionStatusWillChangeNotification";
NSString * const TFSessionStatusDidChangedNotification = @"TFSessionStatusDidChangedNotification";

NSString * const NotificationRefreshNoticeDidUpdated = @"com.ms.NotificationRefreshNoticeDidUpdated";
NSString * const TFNotificationLaunchFromRemoteNotification = @"com.ms.MSNotificationLaunchFromRemoteNotification";


NSString * const TFNotificationUserLogOutSuccess = @"com.tf.TFNotificationUserLogOutSuccess";
NSString * const TFNotificationUserLoginSuccess = @"com.tf.TFNotificationUserLoginSuccess";

@end
