//
//  TFNotificationName.h
//  TFMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TFNotificationName: NSObject

// TFSessionManager
extern NSString * const TFSessionStatusWillChangeNotification;
extern NSString * const TFSessionStatusDidChangedNotification;

extern NSString * const NotificationRefreshNoticeDidUpdated;
extern NSString * const TFNotificationLaunchFromRemoteNotification;



extern NSString * const TFNotificationUserLogOutSuccess;
extern NSString * const TFNotificationUserLoginSuccess;

@end
