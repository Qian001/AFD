//
//  AFDConstValue.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/3.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDConstValue.h"

@implementation AFDConstValue

// TFSessionManager
NSString * const AFDPersistenceSessionConfigurationCookiesData = @"AFDPersistenceSessionConfigurationCookiesData";
NSString * const AFDPersistenceSessionData = @"AFDPersistenceSessionData";

@end
