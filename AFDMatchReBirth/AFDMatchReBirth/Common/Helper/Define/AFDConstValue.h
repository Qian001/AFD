//
//  AFDConstValue.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/3.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFDConstValue : NSObject

// TFSessionManager
extern NSString * const AFDPersistenceSessionConfigurationCookiesData;
extern NSString * const AFDPersistenceSessionData;

@end
