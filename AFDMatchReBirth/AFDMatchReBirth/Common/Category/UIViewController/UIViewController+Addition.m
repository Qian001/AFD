//
//  UIViewController+Addition.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "UIViewController+Addition.h"
#import "AFDViewController.h"

@implementation UIViewController (Addition)

- (UIViewController *)previsousViewController {
    NSArray *viewControllers = self.navigationController.viewControllers;
    NSUInteger controllerCount = viewControllers.count;
    if (controllerCount >1) {
        UIViewController *controller = viewControllers[controllerCount - 2];
        return controller;
    }
    return nil;
}

+ (UIViewController*)topMostController {
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    return [[self class] nextVisibleViewControllerOnViewController:rootViewController];
}

+ (UIViewController *)nextVisibleViewControllerOnViewController:(UIViewController *)inViewController {
    while (inViewController.presentedViewController) {
        inViewController = inViewController.presentedViewController;
    }
    if ([inViewController isKindOfClass:[UITabBarController class]]) {
        UIViewController *selectedVC = [self nextVisibleViewControllerOnViewController:((UITabBarController *)inViewController).selectedViewController];
        return selectedVC;
    } else if ([inViewController isKindOfClass:[UINavigationController class]]) {
        UIViewController *selectedVC = [self nextVisibleViewControllerOnViewController:((UINavigationController *)inViewController).visibleViewController];
        return selectedVC;
    } else {
        return inViewController;
    }
}

@end
