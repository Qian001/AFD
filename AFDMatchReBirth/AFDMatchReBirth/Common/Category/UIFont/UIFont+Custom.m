//
//  UIFont+Custom.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "UIFont+Custom.h"

@implementation UIFont (Custom)

+ (UIFont *)appLightFontOfSize:(CGFloat)size{
    return [UIFont fontWithName:@"SFUIText-Regular" size:size];
}

+ (UIFont *)appFontOfSize:(CGFloat)size{
    return [UIFont fontWithName:@"SFUIText-Regular" size:size];
}

+ (UIFont *)appBoldFontOfSize:(CGFloat)size{
    return [UIFont fontWithName:@"SFUIText-Semibold" size:size];
}

+ (UIFont *)appHeavyFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Semibold" size:size];
}

+ (UIFont *)appExtraLightFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Regular" size:size];
}

+ (UIFont *)appHelveticaBoldFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Semibold" size:size];
}

@end
