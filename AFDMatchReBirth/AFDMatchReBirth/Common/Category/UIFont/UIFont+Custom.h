//
//  UIFont+Custom.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (Custom)

+ (UIFont *)appLightFontOfSize:(CGFloat)size;

+ (UIFont *)appFontOfSize:(CGFloat)size;

+ (UIFont *)appBoldFontOfSize:(CGFloat)size;

+ (UIFont *)appHeavyFontOfSize:(CGFloat)size;

+ (UIFont *)appExtraLightFontOfSize:(CGFloat)size;

+ (UIFont *)appHelveticaBoldFontOfSize:(CGFloat)size;

@end
