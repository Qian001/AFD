//
//  NSDate+Format.m
//  TFMatchReBirth
//
//  Created by Seedy on 2018/9/3.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "NSDate+Format.h"

@implementation NSDate (Format)

- (NSString *)stringWithFormat:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    @try {
        return [formatter stringFromDate:self];
    }
    @catch (NSException *exception) {
        return nil;
    }
}

@end
