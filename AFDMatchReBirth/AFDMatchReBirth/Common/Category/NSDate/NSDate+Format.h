//
//  NSDate+Format.h
//  TFMatchReBirth
//
//  Created by Seedy on 2018/9/3.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Format)

- (NSString *)stringWithFormat:(NSString *)format;

@end
