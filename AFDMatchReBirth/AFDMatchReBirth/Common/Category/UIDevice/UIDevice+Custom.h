//
//  UIDevice+Custom.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (Custom)

+ (NSString *)deviceType;

@end
