//
//  UIDevice+Custom.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "UIDevice+Custom.h"
#import <sys/utsname.h>

@implementation UIDevice (Custom)

+ (NSString *)deviceType {
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceType = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([deviceType isEqualToString:@"iPhone4,1"])  return @"iPhone 4S";
    if ([deviceType isEqualToString:@"iPhone5,1"])  return @"iPhone 5";
    if ([deviceType isEqualToString:@"iPhone5,2"])  return @"iPhone 5";
    if ([deviceType isEqualToString:@"iPhone5,3"])  return @"iPhone 5c";
    if ([deviceType isEqualToString:@"iPhone5,4"])  return @"iPhone 5c";
    if ([deviceType isEqualToString:@"iPhone6,1"])  return @"iPhone 5s";
    if ([deviceType isEqualToString:@"iPhone6,2"])  return @"iPhone 5s";
    if ([deviceType isEqualToString:@"iPhone7,1"])  return @"iPhone 6 Plus";
    if ([deviceType isEqualToString:@"iPhone7,2"])  return @"iPhone 6";
    if ([deviceType isEqualToString:@"iPhone8,1"])  return @"iPhone 6s";
    if ([deviceType isEqualToString:@"iPhone8,2"])  return @"iPhone 6s Plus";
    if ([deviceType isEqualToString:@"iPhone8,4"])  return @"iPhone SE";
    if ([deviceType isEqualToString:@"iPhone9,1"])  return @"iPhone 7";
    if ([deviceType isEqualToString:@"iPhone9,2"])  return @"iPhone 7 Plus";
    if ([deviceType isEqualToString:@"iPhone9,3"])  return @"iPhone 7";
    if ([deviceType isEqualToString:@"iPhone9,4"])  return @"iPhone 7 Plus";
    if ([deviceType isEqualToString:@"iPhone10,1"]) return @"iPhone 8";
    if ([deviceType isEqualToString:@"iPhone10,2"]) return @"iPhone 8 Plus";
    if ([deviceType isEqualToString:@"iPhone10,3"]) return @"iPhone X";
    if ([deviceType isEqualToString:@"iPhone10,4"]) return @"iPhone 8";
    if ([deviceType isEqualToString:@"iPhone10,5"]) return @"iPhone 8 Plus";
    if ([deviceType isEqualToString:@"iPhone10,6"]) return @"iPhone X";
    
    if ([deviceType isEqualToString:@"iPod5,1"])    return @"iPod touch 5G";
    if ([deviceType isEqualToString:@"iPod7,1"])    return @"iPod touch 6G";
    
    if ([deviceType isEqualToString:@"iPad2,1"])    return @"iPad 2";
    if ([deviceType isEqualToString:@"iPad2,2"])    return @"iPad 2";
    if ([deviceType isEqualToString:@"iPad2,3"])    return @"iPad 2";
    if ([deviceType isEqualToString:@"iPad2,4"])    return @"iPad 2";
    if ([deviceType isEqualToString:@"iPad2,5"])    return @"iPad mini";
    if ([deviceType isEqualToString:@"iPad2,6"])    return @"iPad mini";
    if ([deviceType isEqualToString:@"iPad2,7"])    return @"iPad mini";
    if ([deviceType isEqualToString:@"iPad3,1"])    return @"iPad 3";
    if ([deviceType isEqualToString:@"iPad3,2"])    return @"iPad 3";
    if ([deviceType isEqualToString:@"iPad3,3"])    return @"iPad 3";
    if ([deviceType isEqualToString:@"iPad3,4"])    return @"iPad 4";
    if ([deviceType isEqualToString:@"iPad3,5"])    return @"iPad 4";
    if ([deviceType isEqualToString:@"iPad3,6"])    return @"iPad 4";
    if ([deviceType isEqualToString:@"iPad4,1"])    return @"iPad Air";
    if ([deviceType isEqualToString:@"iPad4,2"])    return @"iPad Air";
    if ([deviceType isEqualToString:@"iPad4,3"])    return @"iPad Air";
    if ([deviceType isEqualToString:@"iPad4,4"])    return @"iPad mini 2";
    if ([deviceType isEqualToString:@"iPad4,5"])    return @"iPad mini 2";
    if ([deviceType isEqualToString:@"iPad4,6"])    return @"iPad mini 2";
    if ([deviceType isEqualToString:@"iPad4,7"])    return @"iPad mini 3";
    if ([deviceType isEqualToString:@"iPad4,8"])    return @"iPad mini 3";
    if ([deviceType isEqualToString:@"iPad4,9"])    return @"iPad mini 3";
    if ([deviceType isEqualToString:@"iPad5,1"])    return @"iPad mini 4";
    if ([deviceType isEqualToString:@"iPad5,2"])    return @"iPad mini 4";
    if ([deviceType isEqualToString:@"iPad5,3"])    return @"iPad Air 2";
    if ([deviceType isEqualToString:@"iPad5,4"])    return @"iPad Air 2";
    if ([deviceType isEqualToString:@"iPad6,3"])    return @"iPad Pro (9.7 inch)";
    if ([deviceType isEqualToString:@"iPad6,4"])    return @"iPad Pro (9.7 inch)";
    if ([deviceType isEqualToString:@"iPad6,7"])    return @"iPad Pro (12.9 inch)";
    if ([deviceType isEqualToString:@"iPad6,8"])    return @"iPad Pro (12.9 inch)";
    
    if ([deviceType isEqualToString:@"i386"])       return @"iPhone Simulator";
    if ([deviceType isEqualToString:@"x86_64"])     return @"iPhone Simulator";
    
    return deviceType;
}


@end
