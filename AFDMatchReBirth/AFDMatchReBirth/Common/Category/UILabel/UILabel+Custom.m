//
//  UILabel+Custom.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "UILabel+Custom.h"

@implementation UILabel (Custom)

- (void)sizeWithLimitedWidth:(CGFloat)limitedWidth {
    CGRect orignalRect = self.frame;
    [self sizeToFit];
    UIFont *currentFont = self.font;
    if (limitedWidth > 0 && currentFont.pointSize > 0) {
        if (self.width > limitedWidth) {
            self.font = [UIFont fontWithName:currentFont.fontName size:currentFont.pointSize - 1];
            [self sizeWithLimitedWidth:limitedWidth];
        }
    }
    self.frame = orignalRect;
}

@end
