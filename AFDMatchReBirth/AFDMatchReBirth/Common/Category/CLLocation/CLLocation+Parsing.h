//
//  CLLocation+Parsing.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface CLLocation (Parsing)

- (void)geocodeParser:(void(^)(CLLocation *location, AFDDictionaryItem *country, AFDDictionaryItem *state, AFDDictionaryItem *city, NSString *zip))success failure:(void(^)(CLLocation *location, NSError *error))failure;

@end
