//
//  CLLocation+Parsing.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "CLLocation+Parsing.h"

@implementation CLLocation (Parsing)


- (void)geocodeParser:(void(^)(CLLocation *location, AFDDictionaryItem *country, AFDDictionaryItem *state, AFDDictionaryItem *city, NSString *zip))success
              failure:(void(^)(CLLocation *location, NSError *error))failure {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    NSArray *currentLanguageArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] setObject: [NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
    [geocoder reverseGeocodeLocation:self completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error) {
            if (failure) failure(self, error);
            return;
        }
        [self parserPlacemarks:placemarks success:^(AFDDictionaryItem *country, AFDDictionaryItem *state, AFDDictionaryItem *city, NSString *zip) {
            [[NSUserDefaults standardUserDefaults] setObject:currentLanguageArray forKey:@"AppleLanguages"];
            if(success) success(self, country, state, city, zip);
        } failure:^(NSError *error) {
            [[NSUserDefaults standardUserDefaults] setObject:currentLanguageArray forKey:@"AppleLanguages"];
            if(failure) failure(self, error);
        }];
    }];
}

- (void)parserPlacemarks:(NSArray *)placemarks
                 success:(void(^)(AFDDictionaryItem *country, AFDDictionaryItem *state, AFDDictionaryItem *city, NSString *zip))success
                 failure:(void(^)(NSError *error))failure {
    AFDDictionaryItem *country = nil;
    AFDDictionaryItem *state = nil;
    AFDDictionaryItem *city = nil;
    NSString *enState = nil;
    NSString *enCity = nil;
    NSString *gpsZip = nil;
    for (CLPlacemark *place in placemarks) {
        enState = place.administrativeArea;
        enCity = place.locality;
        gpsZip = place.postalCode;
        
        country = [AFDDictionaryManager countryFromPlacemark:place];
        state = nil;
        if (country) {
            state = [AFDDictionaryManager stateOfCountry:country fromPlacemark:place];
        }
        if (!state && enState) {
            state = [AFDDictionaryItem dictionaryItemWithKey:nil andText:enState];
        }
        if (state && !city && enCity) {
            city = [AFDDictionaryItem dictionaryItemWithKey:nil andText:enCity];
        }
        if (country) break;
    }
    
    if (country) {
        // Only USA 0, Cannada 31 , UK 180 needs to fill zip code.
        NSInteger countryID = country.key.integerValue;
        if (gpsZip && (countryID == 0 || countryID == 31 || countryID == 180)) {
//            MSReqCheckZip *request = [[MSReqCheckZip alloc] init];
//            request.zip = gpsZip;
//            request.country = country;
//            [request request:^(NSURLSessionDataTask *task, id responseObject) {
//                if (success) success(country, state, city, gpsZip);
//            } failure:^(NSURLSessionDataTask *task, NSError *error) {
//                if (failure) failure(nil);
//            }];
            if (success) success(country, state, city, gpsZip);
        } else {
            if (success) success(country, state, city, gpsZip);
        }
    } else {
        if (failure) failure(nil);
    }
}

@end
