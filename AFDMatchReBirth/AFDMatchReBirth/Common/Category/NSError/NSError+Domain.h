//
//  NSError+Domain.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>


#define AFDRequestionErrorDomain @"AFDRequestionErrorDomain"
#define kNSErrorTFErrors @"NSErrorTFErrors"

@interface NSError (Domain)

+ (id)errorWithDomain:(NSString *)domain code:(NSInteger)code message:(NSString *)message;

+ (id)errorWithErrors:(NSArray *)errors;

+ (id)networkError;

- (NSArray *)errors;

- (NSString *)message;

- (BOOL)sessionExpired;

- (BOOL)accountUnavailable;

- (BOOL)responseUnrecognized;

@end

typedef enum TFErrorCode : NSInteger {
    /**
     *  The username is incorrect.
     */
    ErrorCode_Positive_1 = -1,
    
    /**
     *  The password is incorrect.
     */
    ErrorCode_Positive_2 = -2,
    
    /**
     *  Your account has been suspended.
     */
    ErrorCode_Positive_4 = -4,
    
    /**
     *  Your account has been suspended.
     */
    ErrorCode_Positive_6 = -6,
    
    /**
     *  Your account has been suspended.
     */
    ErrorCode_Positive_7 = -7,
    
    /**
     *  You haven't logged in for a long time! Your password has expired. Reset your password to continue.
     */
    ErrorCode_Positive_16 = -16,
    
    /**
     *  Your account was compromised. Please reset password.
     */
    ErrorCode_Positive_18 = -18,
    
    /**
     *  Username cannot be less than 6 characters.
     */
    ErrorCode_5 = 5,
    
    /**
     *  Username cannot contain blank characters.
     */
    ErrorCode_6 = 6,
    
    /**
     *  Password cannot be less than 6 characters.
     */
    ErrorCode_8 = 8,
    
    /**
     *  Password cannot contain blank characters.
     */
    ErrorCode_9 = 9,
    
    /**
     *  Parameter error
     */
    ErrorCode_10 = 10,
    
    /**
     *  Response format error
     */
    ErrorCode_11 = 11,
    
    /**
     *  Please input correct title.
     */
    ErrorCode_24 = 24,
    
    /**
     *  you can not send a wink if your profile is hidden.
     */
    ErrorCode_97 = 97,
    
    /**
     *  You can ONLY wink at the same member once a week.
     */
    ErrorCode_98 = 98,
    
    /**
     *  you can not send a wink for the user block or any other reason.
     */
    ErrorCode_99 = 99,
    
    /**
     *  Session expired.
     */
    ErrorCode_100 = 100,
    
    /**
     *  Your account has been blocked.
     */
    ErrorCode_101 = 101,
    
    /**
     *  User not exist or password error.
     */
    ErrorCode_102 = 102,
    
    /**
     *  1.Payment error for Apple server is not available.
     *  2.login need verify code
     */
    ErrorCode_103 = 103,
    
    /**
     *  1.Payment error for Transaction ID is already used.
     *  2.
     *
     */
    ErrorCode_104 = 104,
    
    /**
     *  Payment error for Transaction is invalid.
     */
    ErrorCode_105 = 105,
    
    /**
     *  Payment error for Product is invalid.
     */
    ErrorCode_106 = 106,
    
    /**
     *  Account is Disabled.
     */
    ErrorCode_107 = 107,
    
    /**
     *  Facebook account didn't register.
     */
    ErrorCode_109 = 109,
    
    /**
     *  Username can't contain inappropiate words
     */
    ErrorCode_110 = 110,
    
    /**
     *  1.Username can only contain a-z, A-Z, 0-9, and _.
     *  2.Verify code wrong
     */
    ErrorCode_111 = 111,
    
    /**
     *  Username and email address name can not be the same.
     */
    ErrorCode_112 = 112,
    
    /**
     *  Username can not only contain digit.
     */
    ErrorCode_113 = 113,
    
    /**
     *  Username is less than 6 characters.
     */
    ErrorCode_114 = 114,
    
    /**
     *  Password is less than 6 characters.
     */
    ErrorCode_115 = 115,
    
    /**
     *  Username  is unavailable. Please try a different username
     */
    ErrorCode_116 = 116,
    
    /**
     *  Username already exists on our system.
     */
    ErrorCode_117 = 117,
    
    /**
     *  Email address already exists on our system.
     */
    ErrorCode_118 = 118,
    
    /**
     *  Viewdme upgrade
     */
    ErrorCode_119 = 119,
    
    /**
     *  Email address did not regist. Reset password failed.
     *  Email address is invalid.
     *  Email address is blocked.
     */
    ErrorCode_120 = 120,
    
    /**
     *  Profile hidden.
     */
    ErrorCode_121 = 121,
    
    /**
     *  The size of photo is not enough to upload.
     */
    ErrorCode_122 = 122,
    /**
     *  User album is full.
     */
    ErrorCode_123 = 123,
    
    /**
     *  User not exsits.
     */
    ErrorCode_125 = 125,
    
    /**
     *  Incorect zip code.
     */
    ErrorCode_126 = 126,
    
    /**
     *  Your account is removed.
     */
    ErrorCode_127 = 127,
    
    /**
     *  Use old user name.
     */
    ErrorCode_128 = 128,
    
    /**
     *  Need update photo before wink.
     */
    ErrorCode_130 = 130,
    
    /**
     *  This moment has been deleted.
     */
    ErrorCode_132 = 132,
    
    /**
     *  Profile pending approval.
     */
    ErrorCode_137 = 137,
    
    /**
     *  You can not comment on this user unless you unblock the use.
     */
    ErrorCode_140 = 140,
    
    /**
     *  Need upload photo before sending a photo request.
     */
    ErrorCode_401 = 401,
    
    /**
     *  The blog has been deleted. Or, moments has been deleted.
     */
    ErrorCode_701 = 701,
    
    /**
     *  The userName has more than 6 digits.
     */
    ErrorCode_10101 = 10101,
    
}TFErrorCode;
