//
//  NSError+Domain.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "NSError+Domain.h"

@implementation NSError (Domain)

+ (id)errorWithDomain:(NSString *)domain code:(NSInteger)code message:(NSString *)message {
    NSDictionary *userInfo = message ? @{NSLocalizedDescriptionKey:message} : nil;
    return [NSError errorWithDomain:domain code:code userInfo:userInfo];
}

+ (id)errorWithErrors:(NSArray *)errors {
    NSDictionary *userInfo = (errors && errors.count > 0) ? @{kNSErrorTFErrors: errors} : nil;
    return [NSError errorWithDomain:@"MultiFailure" code:0 userInfo:userInfo];
}

+ (id)networkError {
    return [NSError errorWithDomain:AFDRequestionErrorDomain code:0 message:NSLocalizedString(@"request_failed", nil)];
}

- (NSArray *)errors {
    return [self.userInfo objectForKey:kNSErrorTFErrors];
}

- (NSString *)message {
    return [self.userInfo objectForKey:NSLocalizedDescriptionKey];
}

- (BOOL)sessionExpired {
    return ([self.domain isEqualToString:AFDRequestionErrorDomain] &&
            self.code == ErrorCode_100);
}

- (BOOL)accountUnavailable {
    return ([self.domain isEqualToString:AFDRequestionErrorDomain] &&
            (self.code == ErrorCode_101 || self.code == ErrorCode_107));
}

- (BOOL)responseUnrecognized {
    return [self.domain isEqualToString:AFDRequestionErrorDomain] && self.code == ErrorCode_10;
}

@end
