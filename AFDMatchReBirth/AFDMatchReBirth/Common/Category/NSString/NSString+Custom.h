//
//  NSString+Custom.h
//  CustomMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Custom)

- (BOOL)isPureInt;

- (BOOL)isTextEmpty;

- (BOOL)isChinese;

+ (CGSize)calculateForString:(NSString *)value font:(UIFont *)font maxWidth:(CGFloat)width;

- (NSString *)sha1;

- (NSString *)md5;

@end
