//
//  NSString+Custiom.m
//  CustomMatchReBirth
//
//  Created by Seedy on 2018/8/24.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "NSString+Custom.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (Custom)

- (BOOL)isPureInt {
    NSScanner* scan = [NSScanner scannerWithString:self];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}

- (BOOL)isTextEmpty {
    if (self == nil || (id)self == [NSNull null]|| [self isEqualToString:@""] || [self isEqualToString:@"(null)"]) {
        return YES;
    }else{
        if (![self respondsToSelector:@selector(length)]) {
            return YES;
        }
        NSString *string = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([string length]) {
            return NO;
        }
    }
    return YES;
}

- (BOOL)isChinese {
    NSString *match = @"(^[\u4e00-\u9fa5]+$)";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF matches %@", match];
    return [predicate evaluateWithObject:self];
}

+ (CGSize)calculateForString:(NSString *)value font:(UIFont *)font maxWidth:(CGFloat)width {
    UILabel *caLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, HUGE)];
    caLabel.font = font;
    caLabel.numberOfLines = 0;
    caLabel.text = value;
    return [caLabel sizeThatFits:CGSizeMake(width, HUGE)];
}

- (NSString *)sha1 {
    const char *str = self.UTF8String;
    // Use corresponding CC_SHA1,CC_SHA256,CC_SHA384,CC_SHA512 The lengths of each of these areThe lengths of each of these are20,32,48,64
    uint64_t digestLegnth = CC_SHA1_DIGEST_LENGTH;
    unsigned char digest[digestLegnth];
    // Use corresponding CC_SHA256,CC_SHA384,CC_SHA512
    CC_SHA1(str, (CC_LONG)strlen(str), digest);
    NSMutableString *output = [NSMutableString stringWithCapacity:digestLegnth * 2];
    for (int i = 0; i < digestLegnth; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    return output;
}

- (NSString *)md5 {
    const char *cStr = [self UTF8String];
    unsigned char result[32];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    NSString *md5 = [NSString stringWithFormat:
                     @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                     result[0], result[1], result[2], result[3],
                     result[4], result[5], result[6], result[7],
                     result[8], result[9], result[10], result[11],
                     result[12], result[13], result[14], result[15]
                     ];
    return [md5 lowercaseString];
}

@end
