//
//  UIView+Loading.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFDLoadingView.h"

@interface UIView (Loading)

@property (nonatomic, strong) AFDLoadingView * loadingview;
@property (nonatomic, assign, getter=isLoading) BOOL loading;

- (void)showLoadingView;

- (void)hideLoadingView;

- (void)showLoadingViewWithTranslucentBackground;

@end
