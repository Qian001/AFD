//
//  UIView+Position.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "UIView+Position.h"

@implementation UIView (Position)

- (void)moveOriginToPointX:(CGFloat)x {
    self.frame = CGRectMake(x, self.frame.origin.y,
                            self.frame.size.width, self.frame.size.height);
}
- (void)moveOriginToPointY:(CGFloat)y {
    self.frame = CGRectMake(self.frame.origin.x, y,
                            self.frame.size.width, self.frame.size.height);
}
- (void)moveOriginToPointX:(CGFloat)x pointY:(CGFloat)y {
    self.frame = CGRectMake(x, y,
                            self.frame.size.width, self.frame.size.height);
}
- (void)moveOriginToPoint:(CGPoint)point {
    self.frame = CGRectMake(point.x, point.y,
                            self.frame.size.width, self.frame.size.height);
}

- (void)moveOriginWithDetalX:(CGFloat)x andDetalY:(CGFloat)y {
    self.frame = CGRectMake(self.frame.origin.x + x,
                            self.frame.origin.y + y,
                            self.frame.size.width,
                            self.frame.size.height);
}

- (void)moveCenterToPoint:(CGPoint)point {
    self.frame = CGRectMake(point.x - CGRectGetWidth(self.frame) / 2,
                            point.y - CGRectGetHeight(self.frame) / 2,
                            CGRectGetWidth(self.frame),
                            CGRectGetHeight(self.frame));
}

- (void)resize:(CGSize)size {
    if(CGSizeEqualToSize(self.frame.size, size) == NO){
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y,
                                size.width, size.height);
    }
}

- (void)resizeToWidth:(CGFloat)width {
    [self resize:CGSizeMake(width, self.frame.size.height)];
}

- (void)resizeToHeight:(CGFloat)height {
    [self resize:CGSizeMake(self.frame.size.width, height)];
}

- (CGPoint)middlePoint {
    CGPoint middlePoint = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    return middlePoint;
}

@end
