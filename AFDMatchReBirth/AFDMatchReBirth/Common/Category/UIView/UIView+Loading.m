//
//  UIView+Loading.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "UIView+Loading.h"
#import <objc/runtime.h>

@implementation UIView (Loading)

- (AFDLoadingView *)loadingview {
    AFDLoadingView * view = objc_getAssociatedObject(self, @selector(loadingview));
    return view;
}

- (void)setLoadingview:(AFDLoadingView *)loadingview {
    objc_setAssociatedObject(self, @selector(loadingview), loadingview, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setLoading:(BOOL)loading {
    NSValue *value = [NSValue value:&loading withObjCType:@encode(BOOL)];
    objc_setAssociatedObject(self, "loading", value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)isLoading {
    NSValue *value = objc_getAssociatedObject(self, "loading");
    BOOL isLoading;
    [value getValue:&isLoading];
    return isLoading;
}

- (void)showLoadingView {
    AFDLoadingView * view = self.loadingview;
    if (!view) {
        view = [[AFDLoadingView alloc] init];
        self.loadingview = view;
    }
    self.loading = YES;
    if (!(view && view.superview)) {
        [self addSubview:self.loadingview];
        [self.loadingview beginAnamtion];
        self.loadingview.translucentBackground = NO;
    }
}

- (void)showLoadingViewWithTranslucentBackground {
    self.loading = YES;
    [self showLoadingView];
    self.loadingview.translucentBackground = YES;
    if ([self isKindOfClass:[UIScrollView class]]) {
        UIScrollView *scrollView = (UIScrollView *)self;
        self.loadingview.frame = CGRectMake(0, 0, scrollView.contentSize.width, scrollView.contentSize.height);
    }
}

- (void)hideLoadingView {
    self.loading = NO;
    [self.loadingview stopAnimation];
    [self.loadingview removeFromSuperview];
}

@end
