//
//  UIView+Position.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Position)

- (void)moveOriginToPointX:(CGFloat)x;
- (void)moveOriginToPointY:(CGFloat)y;
- (void)moveOriginToPointX:(CGFloat)x pointY:(CGFloat)y;
- (void)moveOriginToPoint:(CGPoint)point;
- (void)moveOriginWithDetalX:(CGFloat)x andDetalY:(CGFloat)y;

- (void)moveCenterToPoint:(CGPoint)point;

- (void)resize:(CGSize)size;
- (void)resizeToWidth:(CGFloat)width;
- (void)resizeToHeight:(CGFloat)height;
- (CGPoint)middlePoint;

@end
