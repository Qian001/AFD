//
//  UIView+Frame.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/8/31.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)

@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat x, centerX;
@property (nonatomic, assign) CGFloat y, centerY;
@property (nonatomic, assign, readonly) CGPoint boundsCenter;
@property (nonatomic, assign, readonly) CGPoint visibleCenter;

@property (nonatomic, assign) CGFloat top;
@property (nonatomic, assign) CGFloat bottom;
@property (nonatomic, assign) CGFloat left;
@property (nonatomic, assign) CGFloat right;

@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGPoint origin;

- (CGFloat)minX;

- (CGFloat)midX;

- (CGFloat)maxX;

- (CGFloat)minY;

- (CGFloat)midY;

- (CGFloat)maxY;

@end
