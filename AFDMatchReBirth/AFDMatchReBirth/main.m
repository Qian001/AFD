//
//  main.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/4.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
