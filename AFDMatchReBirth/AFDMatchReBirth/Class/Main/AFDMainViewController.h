//
//  AFDMainViewController.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDViewController.h"
#import "AFDMessageViewController.h"
#import "MyProfileViewController.h"
#import "AFDFindViewController.h"

@interface AFDMainViewController : UITabBarController

@property (nonatomic, strong) NSDate *tapButtonTime;

@property (nonatomic, weak) UIViewController *prevSelectedViewController;

@property (nonatomic, strong) UINavigationController *starNavigationController;
@property (nonatomic, strong) UINavigationController *findNavigationController;
@property (nonatomic, strong) UINavigationController *messageNavigationController;
@property (nonatomic, strong) UINavigationController *profileNavigationController;

@property (nonatomic, strong) UIViewController *currentConnectionController;

- (void)setUpMenuController;
- (NSInteger)indexOfMenuNavigationController:(UINavigationController *)navigationController;

- (void)setBadge;
- (void)handleLaunchFromRemoteNotification:(NSNotification *)notification;

- (void)memberShipChangedNotificationHandler;
- (void)handleMSNotificationCheckSearchResultsPage:(NSNotification *)notification;
- (void)backToControllerWithIndex:(NSInteger)index;

@end
