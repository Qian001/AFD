//
//  AFDMainViewController.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDMainViewController.h"

@interface AFDMainViewController ()

@end

@implementation AFDMainViewController{
    BOOL isShowProfilePage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpMenuController];
    self.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBar.translucent = NO;
    self.tabBar.backgroundImage = [UIImage new];
    self.tabBar.shadowImage = [UIImage new];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0) {
        self.tabBarController.tabBar.subviews[0].subviews[1].hidden = YES;
    }
    
    [self setBadge];
    isShowProfilePage = YES;
//    [MSRateUsManager sharedInstance].delegate = self;
    
    [[NSNotificationCenter defaultCenter ] addObserver:self selector:@selector(setBadge) name:NotificationRefreshNoticeDidUpdated object:nil];
//    [[NSNotificationCenter defaultCenter ] addObserver:self selector:@selector(handleLaunchFromRemoteNotification:) name:MSNotificationLaunchFromRemoteNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMSNotificationCheckSearchResultsPage:)                                                  name:MSNotificationCheckSearchResultsPage object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(memberShipChangedNotificationHandler) name:MSNotificationMembershipChanged object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotificationRemoteSyncMyProfile) name:MSNotificationRemoteSyncMyProfile object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleCompleteProfileNotification) name:MSNotificationCompleteProfile object:nil];
//    [[NSNotificationCenter defaultCenter ] addObserver:self selector:@selector(handleMSNotificationCheckSearchResultsPage:) name:MSNotificationShouldGoToBowseView object:nil];
    [self dropShadowWithOffset:CGSizeMake(0, -3)
                        radius:3
                         color:[UIColor blackColor]
                       opacity:0.1];
}

- (void)dropShadowWithOffset:(CGSize)offset
                      radius:(CGFloat)radius
                       color:(UIColor *)color
                     opacity:(CGFloat)opacity {
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, self.tabBar.bounds);
    self.tabBar.layer.shadowPath = path;
    CGPathCloseSubpath(path);
    CGPathRelease(path);
    self.tabBar.layer.shadowColor = color.CGColor;
    self.tabBar.layer.shadowOffset = offset;
    self.tabBar.layer.shadowRadius = radius;
    self.tabBar.layer.shadowOpacity = opacity;
    self.tabBar.clipsToBounds = NO;
}

- (void)setUpMenuController {
    NSMutableArray * viewControllers = [@[] mutableCopy];
    self.starNavigationController = [self setNavigationController:[UIViewController new] WithTitle:nil image:[[UIImage imageNamed:@"tabar_mouments"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectImage:[[UIImage imageNamed:@"tabar_mouments"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [viewControllers addObject:self.starNavigationController];
    
    self.findNavigationController = [self setNavigationController:[AFDFindViewController new] WithTitle:nil image:[[UIImage imageNamed:@"tabar_mouments"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectImage:[[UIImage imageNamed:@"tabar_mouments"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [viewControllers addObject:self.findNavigationController];
    
    self.messageNavigationController = [self setNavigationController:[AFDMessageViewController new] WithTitle:nil image:[[UIImage imageNamed:@"tabar_mouments"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectImage:[[UIImage imageNamed:@"tabar_mouments"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [viewControllers addObject:self.messageNavigationController];
    
    self.profileNavigationController = [self setNavigationController:[MyProfileViewController new] WithTitle:nil image:[[UIImage imageNamed:@"tabar_mouments"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]selectImage:[[UIImage imageNamed:@"tabar_mouments"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [viewControllers addObject:self.profileNavigationController];
    self.viewControllers = [viewControllers copy];
}

-(UINavigationController *)setNavigationController:(UIViewController *)vc WithTitle:(NSString *)title image:(UIImage *)normalImage selectImage:(UIImage *)selectImage{
    UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:vc];
    vc.tabBarItem.image = normalImage;
    vc.tabBarItem.selectedImage = selectImage;
    vc.tabBarItem.title = title;
    vc.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    if (@available(iOS 10.0, *)) {
        vc.tabBarItem.badgeColor = APP.appMainColor;
    }
    return nav;
}

- (void)handleMSNotificationCheckSearchResultsPage:(NSNotification *)notification {
    [self backToControllerWithIndex:[self indexOfMenuNavigationController:self.findNavigationController]];
}

//- (void)handleLaunchFromRemoteNotification:(NSNotification *)notification {
//    MSNotificationType type = [MSRemoteNotificationManager sharedManager].launchRemoteNotification.type;
//    NSUInteger selectedIndex = self.selectedIndex;
//
//}
//
//- (void)memberShipChangedNotificationHandler{
//    if (![MSProfileManager sharedProfileManager].profile.status.isGold) {
//    } else {
//        [self removeUpgradeIcon];
//    }
//}

- (void)removeUpgradeIcon {
    [self.viewControllers enumerateObjectsUsingBlock:^(__kindof UIViewController * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[UINavigationController class]]) {
            UINavigationController * nav = (UINavigationController *)obj;
            nav.viewControllers.firstObject.navigationItem.leftBarButtonItem = nil;
        }
    }];
}

- (void)setBadge {
//    MSMyProfileModel *profile = [MSProfileManager sharedProfileManager].profile;
//
//    NSInteger index = NSNotFound, badgeCount = 0;
//    UITabBarItem *tabBarItem;
//
//    index = [self indexOfMenuNavigationController:self.searchNavigationController];
//    if (index != NSNotFound) {
//
//    }
//
//    index = [self indexOfMenuNavigationController:self.starNavigationController];
//    if (index != NSNotFound) {
//
//    }
//
//    index = [self indexOfMenuNavigationController:self.messageNavigationController];
//    if (index != NSNotFound) {
//        tabBarItem = [self.tabBar.items objectAtIndex:index];
//        [[MSXMPPServiceCoreDataManager shareManager] queryNumberOfTotalUnreadMessages:^(NSUInteger count) {
//            [tabBarItem setDotBadge:count];
//        }];
//    }
//
//    index = [self indexOfMenuNavigationController:self.profileNavigationController];
//    if (index != NSNotFound) {
//        badgeCount = profile.privateAlbumRequestNewNum;
//        tabBarItem = [self.tabBar.items objectAtIndex:index];
//        [tabBarItem setDotBadge:badgeCount];
//    }
}

//- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
//    UINavigationController *navigationController = (MSNavigationController *)viewController;
//    [(MSViewController *)navigationController.viewControllers[0] refreshSuspendedView];
//    if (viewController == self.prevSelectedViewController) {
//        if ([[NSDate date] timeIntervalSinceDate:self.tapButtonTime] <= 1) {
//            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationTapMenuItemAgain object:nil userInfo:@{@"viewController" : viewController, @"doubleTap": @(1)}];
//        } else {
//            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationTapMenuItemAgain object:nil userInfo:@{@"viewController" : viewController}];
//        }
//        self.tapButtonTime = [NSDate date];
//    } else if (((UINavigationController *)self.prevSelectedViewController).viewControllers[0] == self.currentConnectionController) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:MSNotificationChangeMainTab object:nil];
//    }
//}
//
//- (void)handleNotificationRemoteSyncMyProfile {
//    if (![[MSProfileManager sharedProfileManager] shouldAskOpenProfile]) {
//        //        [self showCompleteProfileAlert];
//    }
//}

- (void)backTosearchNavigationController {
    [self backToControllerWithIndex:[self indexOfMenuNavigationController:self.findNavigationController]];
}

- (void)backTostarNavigationController {
    [self backToControllerWithIndex:[self indexOfMenuNavigationController:self.starNavigationController]];
}

- (void)backToMessageNavigationController {
    [self backToControllerWithIndex:[self indexOfMenuNavigationController:self.messageNavigationController]];
}

- (void)backToProfileNavigationController {
    [self backToControllerWithIndex:[self indexOfMenuNavigationController:self.profileNavigationController]];
}

//- (void)backToControllerWithIndex:(NSInteger)index {
//    if (index == NSNotFound) {
//        UIViewController *browseViewController = [APP_CLASS_PRODUCER browseViewController];
//        if (self.navigationController) {
//            [self.navigationController pushViewController:browseViewController animated:YES];
//        } else {
//            UINavigationController *nav = self.selectedViewController;
//            if ([nav isKindOfClass:[UINavigationController class]]) {
//                [nav pushViewController:browseViewController animated:YES];
//            }
//        }
//        return;
//    }
//
//    self.selectedIndex = index;
//    UIViewController *selectedViewController = self.selectedViewController;
//    UINavigationController *navigationController;
//    if ([selectedViewController isKindOfClass:[UINavigationController class]]) {
//        navigationController = (UINavigationController *)selectedViewController;
//        [navigationController popToRootViewControllerAnimated:NO];
//    }
//}

- (NSInteger)indexOfMenuNavigationController:(UINavigationController *)navigationController {
    return [self.viewControllers indexOfObject:navigationController];
}

//- (void)showMenuAction:(id)sender {
//    [[NSNotificationCenter defaultCenter]postNotificationName:MSNotificationSliderTabBarShowMenu object:nil];
//}

#pragma mark - Blog

- (void)handlePostBlogSucceedNotification:(NSNotification *)NSNotification {
    //    [self backToBlogNavigationController];
}

#pragma mark - MSRateUsManagerDelegate
//- (void)rateUsManagerDidChosenFeedBack:(MSRateUsManager *)rateUsManager {
//    [self toFeedbackWithTopicKey:@"8"];
//}
//
//- (void)toFeedbackWithTopicKey:(NSString *)key {
//    MSFeedbackViewController *vc = [[MSFeedbackViewController alloc] init];
//    if (key.length) {
//        NSArray *topics = [MSDictionaryManager feedbackTopicDictionary];
//        MSDictionaryItem *topic = [MSDictionaryManager itemForKey:key atArray:topics];
//        vc.cellText = topic.text;
//    }
//    [self.selectedViewController pushViewController:vc animated:YES];
//}

- (void)handleCompleteProfileNotification {
    if ([self indexOfMenuNavigationController:self.messageNavigationController] != NSNotFound) {
        [self backToControllerWithIndex:[self indexOfMenuNavigationController:self.messageNavigationController]];
    }
}

@end
