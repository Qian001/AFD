//
//  AFDRootViewController.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDRootViewController.h"
#import "AFDHomeViewController.h"
#import "AFDMainViewController.h"

@interface AFDRootViewController ()

@property (nonatomic, strong) AFDMainViewController *menuViewController;
@property (nonatomic, strong) AFDHomeViewController *homeViewController;

@property (nonatomic, assign) BOOL exchanging;
@property (nonatomic, strong) UIViewController *currentViewController;

@end

@implementation AFDRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showHomeController) name:TFNotificationUserLogOutSuccess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showMenuController) name:TFNotificationUserLoginSuccess object:nil];
    
    [self showMenuController];
}


- (void)showMenuController {
    if (self.exchanging || (self.currentViewController && self.currentViewController == _menuViewController)) {
        return;
    }
    [self showMainFrameView];
}
- (void)showHomeController {
    if (self.exchanging || (self.currentViewController && self.currentViewController == self.homeViewController)) {
        return;
    }
    self.exchanging = YES;
    CGRect rect = self.view.bounds;
    if (self.currentViewController) {
        rect.origin.y = rect.size.height;
        self.homeViewController.view.frame = rect;
        [self addChildViewController:self.homeViewController];
        [self.view addSubview:self.homeViewController.view];
        [self.homeViewController didMoveToParentViewController:self];
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.homeViewController.view.frame = self.view.bounds;
        } completion:^(BOOL finished) {
            [self.currentViewController willMoveToParentViewController:nil];
            [self.currentViewController.view removeFromSuperview];
            [self.currentViewController removeFromParentViewController];
            self.currentViewController = self.homeViewController;
            self.menuViewController = nil;
            self.exchanging = NO;
            [self dismissViewControllerAnimated:YES toViewController:self completion:^{
               
            }];
        }];
    } else {
        self.homeViewController.view.frame = rect;
        [self addChildViewController:self.homeViewController];
        [self.view addSubview:self.homeViewController.view];
        [self.homeViewController didMoveToParentViewController:self];
        self.currentViewController = self.homeViewController;
        self.menuViewController = nil;
        self.exchanging = NO;
        [self dismissViewControllerAnimated:YES toViewController:self completion:^{
           
        }];
    }
}

- (void)dismissViewControllerAnimated:(BOOL)animated toViewController:(UIViewController *)toViewController completion:(void (^)(void))completion {
    __weak typeof(self) wSelf = self;
    UIViewController *topMostController = [UIViewController topMostController] ?: self;
    if (topMostController && topMostController != toViewController) {
        if (topMostController.presentedViewController) {
            [topMostController dismissViewControllerAnimated:animated completion:^{
                [wSelf dismissViewControllerAnimated:animated toViewController:toViewController completion:completion];
            }];
        } else {
            if (completion) completion();
        }
    } else {
        if (completion) completion();
    }
}

- (void)showMainFrameView {
    self.exchanging = YES;
    CGRect rect = self.view.bounds;
    self.menuViewController.view.frame = rect;
    if (self.currentViewController) {
        [self addChildViewController:self.menuViewController];
        [self.view insertSubview:self.menuViewController.view belowSubview:self.currentViewController.view];
        [self.menuViewController didMoveToParentViewController:self];
        rect.origin.y = rect.size.height;
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.currentViewController.view.frame = rect;
        } completion:^(BOOL finished) {
            [self.currentViewController willMoveToParentViewController:nil];
            [self.currentViewController.view removeFromSuperview];
            [self.currentViewController removeFromParentViewController];
            self.currentViewController = self.menuViewController;
            self.homeViewController = nil;
            self.exchanging = NO;
            [self setNeedsStatusBarAppearanceUpdate];
        }];
    } else {
        [self addChildViewController:self.menuViewController];
        [self.view addSubview:self.menuViewController.view];
        [self.menuViewController didMoveToParentViewController:self];
        self.currentViewController = self.menuViewController;
        self.homeViewController = nil;
        self.exchanging = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self setNeedsStatusBarAppearanceUpdate];
        });
    }
}

- (AFDMainViewController *)menuViewController {
    if (!_menuViewController) {
        _menuViewController = [[AFDMainViewController alloc]init];
    }
    return _menuViewController;
}

- (AFDHomeViewController *)homeViewController {
    if (!_homeViewController) {
        _homeViewController = [[AFDHomeViewController alloc]init];
    }
    return _homeViewController;
}

@end
