//
//  AFDHomeViewController.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDHomeViewController.h"

#import "AFDBasePresentView.h"
#import "AFDPresentAlertView.h"
#import "AFDLocationManager.h"

#import "AFDMyProfileModel.h"

@interface AFDHomeViewController ()

@end

@implementation AFDHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:self.view.bounds];
    titleLabel.text = @"TFHomeViewController";
    [self.view addSubview:titleLabel];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        AFDPresentAlertView *pressentAlertView = [[AFDPresentAlertView alloc] initWithButtonItems:@[[AFDAlertButtonItem buttonItemWithTitle:@"Yes" action:^{
        
           
        } bold:NO]]];
        pressentAlertView.titleLabel.text = @"Want to sign out?";
        pressentAlertView.textLabel.text = @"Logout text alert";
        pressentAlertView.tintColor = APP.appMainColor;
        
        [pressentAlertView setDismissCompeltion:^{
            
        }];
        [pressentAlertView presentAnimated:YES completion:nil];
    });
    
//    TFBasePresentView *presentView =  [[TFBasePresentView alloc]init];
//    [presentView presentAnimated:true completion:^{
//        
//    }];
    
    NSArray *genders = [AFDDictionaryManager genderDictionaryItems];
    for (AFDDictionaryItem *item in genders) {
        NSLog(@"%@---%@",item.key,item.text);
    }
    
    AFDLocationManager *locationMan = [[AFDLocationManager alloc]init];
    [locationMan startLocateGeocodeParser:^(CLLocation *location, AFDDictionaryItem *country, AFDDictionaryItem *state, AFDDictionaryItem *city, NSString *zip) {
        NSLog(@"country = %@,state = %@,city = %@,zip = %@",country.text,state.text,city.text,zip);
    } locateFailure:^(NSError *error) {
        
    } geocodeFailure:^(CLLocation *location, NSError *error) {
        
    }];
    
    NSString *str = [AFDAppInfoManager appTid];
    NSLog(@"%@-----",str);
    

    [[AFDMyProfileModel shareInstance]updateUserProfile];
    
    NSLog(@"---%@---",[[AFDMyProfileModel shareInstance]wcdbMyprofile].userModel.name);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
