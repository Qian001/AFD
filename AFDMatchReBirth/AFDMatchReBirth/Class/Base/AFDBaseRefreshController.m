//
//  AFDBaseRefreshController.m
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDBaseRefreshController.h"
#import "AFDDIYHeader.h"
#import "AFDDIYFooter.h"

@interface AFDBaseRefreshController ()

@end

@implementation AFDBaseRefreshController

- (instancetype)init {
    if (self = [super init]) {
        self.loadMore = true;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self refreshDataView].mj_header = [AFDDIYHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    [self refreshDataView].mj_footer = [AFDDIYFooter footerWithRefreshingTarget:self refreshingAction:@selector(lodaMoreData)];
}

- (void)beginRefresh {
    [self loadNewData];
}

- (void)endRefresh {
    [[self refreshDataView].mj_footer endRefreshing];
    [[self refreshDataView].mj_header endRefreshing];
}

- (void)loadNewData {
    [self dataSourceRefreshWithCompletion:^(NSURLSessionDataTask *task, NSError *error) {
        [self endRefresh];
    }];
}

- (void)lodaMoreData {
    [self dataSourceLoadMoreWithCompletion:^(NSURLSessionDataTask *task, NSError *error) {
        if (!self.isLoadMore) {
            [[self refreshDataView].mj_footer setState:MJRefreshStateNoMoreData];
        }else {
            [self endRefresh];
        }
    }];
}

- (NSURLSessionDataTask *)dataSourceRefreshWithCompletion:(void (^)(NSURLSessionDataTask *, NSError *))completion {
    return nil;
}

- (NSURLSessionDataTask *)dataSourceLoadMoreWithCompletion:(void (^)(NSURLSessionDataTask *, NSError *))completion {
    return nil;
}

- (UIScrollView *)refreshDataView {
    return nil;
}

@end
