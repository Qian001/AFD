//
//  AFDBaseRefreshController.h
//  AFDMatchReBirth
//
//  Created by Seedy on 2018/9/1.
//  Copyright © 2018年 Seedy. All rights reserved.
//

#import "AFDViewController.h"

@interface AFDBaseRefreshController : AFDViewController

@property (nonatomic, assign, getter=isLoadMore) BOOL loadMore;


- (UIScrollView *)refreshDataView;

- (void)beginRefresh;

- (NSURLSessionDataTask *)dataSourceRefreshWithCompletion:(void(^)(NSURLSessionDataTask *task, NSError *error))completion;

- (NSURLSessionDataTask *)dataSourceLoadMoreWithCompletion:(void(^)(NSURLSessionDataTask *task, NSError *error))completion;

@end
